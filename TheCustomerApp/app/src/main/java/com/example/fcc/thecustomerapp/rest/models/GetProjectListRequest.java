package com.example.fcc.thecustomerapp.rest.models;

import com.example.fcc.thecustomerapp.components.Constants;
import com.google.gson.annotations.SerializedName;


public class GetProjectListRequest {

    @SerializedName(Constants.JSON_SOLICITUDE)
    private String request;
    @SerializedName(Constants.JSON_ID_USER)
    private String userId;

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
