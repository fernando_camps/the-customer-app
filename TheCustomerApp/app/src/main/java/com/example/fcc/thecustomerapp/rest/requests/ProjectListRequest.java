package com.example.fcc.thecustomerapp.rest.requests;

import com.example.fcc.thecustomerapp.rest.models.GetProjectListRequest;
import com.example.fcc.thecustomerapp.rest.models.GetProjectListResponse;
import com.example.fcc.thecustomerapp.rest.webservices.ProjectListWebService;

/**
 * Created by Carlos Briseño on 27/01/2016.
 */
public class ProjectListRequest extends BaseRequest<GetProjectListResponse, ProjectListWebService>{

    private GetProjectListRequest projectListRequest;

    public ProjectListRequest(String request, String userId){
        super(GetProjectListResponse.class, ProjectListWebService.class);

        projectListRequest = new GetProjectListRequest();

        projectListRequest.setRequest(request);
        projectListRequest.setUserId(userId);
    }

    @Override
    public GetProjectListResponse loadDataFromNetwork() throws Exception {
        return getService().getProjectList(projectListRequest);
    }
}
