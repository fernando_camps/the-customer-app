package com.example.fcc.thecustomerapp.models;

import com.example.fcc.thecustomerapp.components.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fcc on 21/01/16.
 */
public class Rate implements Comparable {

    @SerializedName(Constants.JSON_TECNOLOGYS_ID)
    private String id;
    @SerializedName(Constants.JSON_TECNOLOGYS_TYPE_TECNOLOGY)
    private String tecnology;
    @SerializedName(Constants.JSON_TECNOLOGYS_SPRINT)
    private String sprint;
    @SerializedName(Constants.JSON_TECNOLOGYS_RESTRICTIONS)
    private String restrictions;
    @SerializedName(Constants.JSON_TECNOLOGYS_BEGIN)
    private String begin;
    @SerializedName(Constants.JSON_TECNOLOGYS_NEXT_DELIVER)
    private String nextDelivery;
    @SerializedName(Constants.JSON_TECNOLOGYS_STATUS)
    private String status;
    @SerializedName(Constants.JSON_TECNOLOGYS_TYPE)
    private String type;
    @SerializedName(Constants.JSON_TECNOLOGYS_RATE)
    private int rate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTecnology() {
        return tecnology;
    }

    public void setTecnology(String tecnology) {
        this.tecnology = tecnology;
    }

    public String getSprint() {
        return sprint;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public void setSprint(String sprint) {
        this.sprint = sprint;
    }

    public String getRestrictions() {
        return restrictions;
    }

    public void setRestrictions(String restrictions) {
        this.restrictions = restrictions;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getNextDelivery() {
        return nextDelivery;
    }

    public void setNextDelivery(String nextDelivery) {
        this.nextDelivery = nextDelivery;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int compareTo(Object o) {
        Rate rate = (Rate) o;
        return sprint.compareTo(rate.getSprint());
    }
}
