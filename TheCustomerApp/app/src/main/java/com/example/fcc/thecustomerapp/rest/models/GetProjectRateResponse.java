package com.example.fcc.thecustomerapp.rest.models;

import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.models.Rate;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by fcc on 16/02/16.
 */
public class GetProjectRateResponse {
    @SerializedName(Constants.JSON_CODE)
    private int code;
    @SerializedName(Constants.JSON_MSG)
    private String message;
    @SerializedName(Constants.JSON_SOLICITUDE_QUALIFY)
    private List<Rate> rates;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Rate> getRates() {
        return rates;
    }

    public void setRates(List<Rate> rates) {
        this.rates = rates;
    }
}
