package com.example.fcc.thecustomerapp.rest.webservices;

import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.rest.models.GetProjectDetailsRequest;
import com.example.fcc.thecustomerapp.rest.models.GetProjectDetailsResponse;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by fcc on 15/02/16.
 */
public interface DetailsWebService {
    @POST(Constants.URL_DETAILS)
    GetProjectDetailsResponse getDetails(@Body GetProjectDetailsRequest body);
}
