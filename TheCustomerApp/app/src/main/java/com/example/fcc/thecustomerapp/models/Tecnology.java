package com.example.fcc.thecustomerapp.models;

import com.example.fcc.thecustomerapp.components.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fcc on 14/01/16.
 */
public class Tecnology {
    private String day;
    private String month;
    private String year;

    @SerializedName(Constants.JSON_TECNOLOGYS_ID)
    private String id;
    @SerializedName(Constants.JSON_TECNOLOGYS_TYPE_TECNOLOGY)
    private String tecnology;
    @SerializedName(Constants.JSON_TECNOLOGYS_SPRINT)
    private String sprint;
    @SerializedName(Constants.JSON_TECNOLOGYS_RESTRICTIONS)
    private String restrictions;
    @SerializedName(Constants.JSON_TECNOLOGYS_BEGIN)
    private String begin;
    @SerializedName(Constants.JSON_TECNOLOGYS_NEXT_DELIVER)
    private String nextDelivery;
    @SerializedName(Constants.JSON_TECNOLOGYS_STATUS)
    private String status;
    @SerializedName(Constants.JSON_TECNOLOGYS_TYPE)
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTecnology() {
        return tecnology;
    }

    public void setTecnology(String tecnology) {
        this.tecnology = tecnology;
    }

    public String getSprint() {
        return sprint;
    }

    public void setSprint(String sprint) {
        this.sprint = sprint;
    }

    public String getRestrictions() {
        return restrictions;
    }

    public void setRestrictions(String restrictions) {
        this.restrictions = restrictions;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getNextDelivery() {
        return nextDelivery;
    }

    public void setNextDelivery(String nextDelivery) {
        this.nextDelivery = nextDelivery;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFormmatedDate(String date){
        day = date.substring(8, 10);
        month = date.substring(5, 7);
        year = date.substring(0, 4);
        date = (day + Constants.DETAILS_DATE_SEPARATOR + month + Constants.DETAILS_DATE_SEPARATOR + year);

        return date;
    }
}
