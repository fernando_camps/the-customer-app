package com.example.fcc.thecustomerapp.fragments;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.fcc.thecustomerapp.R;
import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.components.InterfaceMain;
import com.example.fcc.thecustomerapp.models.Contact;
import com.example.fcc.thecustomerapp.models.Project;
import com.example.fcc.thecustomerapp.models.Tecnology;
import com.example.fcc.thecustomerapp.models.UserLogin;
import com.example.fcc.thecustomerapp.rest.common.SpiceHelper;
import com.example.fcc.thecustomerapp.rest.models.GetProjectDetailsResponse;
import com.example.fcc.thecustomerapp.rest.requests.DetailsRequest;
import com.example.fcc.thecustomerapp.utilities.Util;
import com.octo.android.robospice.persistence.exception.SpiceException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.activity_fragment_details)
public class FragmentDetails extends BaseRestFragment implements View.OnClickListener, LocationListener{

    //region Variables

    //region Constants X
    //endregion

    //region Primitives X
    //endregion

    //region Objects

    private InterfaceMain listenerMain;
    private UserLogin userLogin;
    private Project proyectList;
    private SpiceHelper<GetProjectDetailsResponse> spiceProjectDetails;
    private Bitmap loadedImage;

    //endregion

    //region Views

    @ViewById
    LinearLayout  lyTecAndroid;
    @ViewById
    LinearLayout  lyTecIos;
    @ViewById
    LinearLayout  lyTecWeb;
    @ViewById
    LinearLayout  layoutDetails;

    @ViewById
    TextView tvDateBeginDetails;
    @ViewById
    TextView tvDateEndDetails;
    @ViewById
    TextView tvDetailsNameProyect;
    @ViewById
    TextView tvDetailsComments;

    @ViewById
    TextView tvNameScrumMaster;
    @ViewById
    TextView tvRoleScrumMaster;
    @ViewById
    TextView tvEmailScrumMaster;

    @ViewById
    TextView tvSprintAndroid;
    @ViewById
    TextView tvSprintAndroidBegin;
    @ViewById
    TextView tvSprintAndroidNextDeliver;
    @ViewById
    TextView tvSprintAndroidTecnology;

    @ViewById
    TextView tvSprintIos;
    @ViewById
    TextView tvSprintIosBegin;
    @ViewById
    TextView tvSprintIosNextDeliver;
    @ViewById
    TextView tvSprintIosTecnology;

    @ViewById
    TextView tvSprintWeb;
    @ViewById
    TextView tvSprintWebBegin;
    @ViewById
    TextView tvSprintWebNextDeliver;
    @ViewById
    TextView tvSprintWebTecnology;

    @ViewById
    ImageView ivPhotoProyect;


    //endregion

    //endregion

    //region Component life cycle

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem menuItem = menu.findItem(R.id.action_email_list);
        menuItem.setVisible(false);
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.activity_fragment_details, container, false);
    }

    @AfterViews
    public void main(){
        setHasOptionsMenu(true);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        lyTecAndroid.setVisibility(View.GONE);
        lyTecWeb.setVisibility(View.GONE);
        lyTecIos.setVisibility(View.GONE);
        layoutDetails.setVisibility(View.GONE);

        spiceProjectDetails = new SpiceHelper<GetProjectDetailsResponse>(getSpiceManager(), "CACHE_LIST", GetProjectDetailsResponse.class) {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                Util.message(Constants.ERROR_ROBOSPICE_PETICION);
                exception.printStackTrace();

                if (listenerMain != null) {
                    listenerMain.hideProgress();
                }

                Util.errorMessage(getContext(), 2);
            }

            @Override
            protected void onSuccess(GetProjectDetailsResponse response) {
                super.onSuccess(response);

                int code = response.getCode();

                if (code == 200) {

                    Project project = new Project();
                    project = response.getProject();

                    Contact contact = new Contact();
                    contact = project.getContact();

                //TODO:   // Falta URL DE LOS SERVICIOS PARA PROBAR
                    if (!project.getImageURL().equals("URL")) {
                        Util.downloadFile(project.getImageURL(), ivPhotoProyect, loadedImage, getContext());
                        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(Constants.TITLE_TOOLBAR_DETAILS);

                    } else {
                        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("   "+project.getTitleProyect());
                        ivPhotoProyect.setVisibility(View.GONE);


                    }

                    tvDetailsNameProyect.setText(project.getTitleProyect());

                    if (project.getDateBegin().length() != 0) {
                        tvDateBeginDetails.setText(project.getFormmatedDate(project.getDateBegin()));
                    }

                    if (project.getDateEnd().length() != 0) {
                        tvDateEndDetails.setText(project.getFormmatedDate(project.getDateEnd()));
                    }

                    tvDetailsComments.setText(project.getComments());

                    tvNameScrumMaster.setText(contact.getName().toUpperCase());
                    tvRoleScrumMaster.setText(contact.getRole());

                    SpannableString content = new SpannableString(contact.getEmail());
                    content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                    tvEmailScrumMaster.setTextColor(Color.CYAN);
                    tvEmailScrumMaster.setText(content);


                    for (int i = 0; i < project.getProjectList().size(); i++) {

                        Tecnology projectListElement = new Tecnology();
                        projectListElement = project.getProjectList().get(i);

                        //TECNOLOGY ANDROID
                        if (projectListElement.getTecnology().equals(Constants.TEC_ANDROID)) {
                            lyTecAndroid.setVisibility(View.VISIBLE);
                            tvSprintAndroid.setText(projectListElement.getSprint());

                            if (projectListElement.getBegin().length() != 0) {
                                tvSprintAndroidBegin.setText(projectListElement.getFormmatedDate(projectListElement.getBegin()));
                            }

                            if (projectListElement.getNextDelivery().length() != 0) {
                                tvSprintAndroidNextDeliver.setText(projectListElement.getFormmatedDate(projectListElement.getNextDelivery()));
                            }

                            tvSprintAndroidTecnology.setText(projectListElement.getTecnology());

                        }

                        //TECNOLOGY IOS
                        if (projectListElement.getTecnology().equals(Constants.TEC_IOS)) {
                            lyTecIos.setVisibility(View.VISIBLE);
                            tvSprintIos.setText(projectListElement.getSprint());

                            if (projectListElement.getBegin().length() != 0) {
                                tvSprintIosBegin.setText(projectListElement.getFormmatedDate(projectListElement.getBegin()));
                            }

                            if (projectListElement.getNextDelivery().length() != 0) {
                                tvSprintIosNextDeliver.setText(projectListElement.getFormmatedDate(projectListElement.getNextDelivery()));
                            }

                            tvSprintIosTecnology.setText(projectListElement.getTecnology());
                        }

                        //TECNOLOGY WEB
                        if (projectListElement.getTecnology().equals(Constants.TEC_WEB)) {
                            lyTecWeb.setVisibility(View.VISIBLE);
                            tvSprintWeb.setText(projectListElement.getSprint());

                            if (projectListElement.getBegin().length() != 0) {
                                tvSprintWebBegin.setText(projectListElement.getFormmatedDate(projectListElement.getBegin()));
                            }

                            if (projectListElement.getNextDelivery().length() != 0) {
                                tvSprintWebNextDeliver.setText(projectListElement.getFormmatedDate(projectListElement.getNextDelivery()));
                            }

                            tvSprintWebTecnology.setText(projectListElement.getTecnology());
                        }
                    }

                    if (listenerMain != null) {
                        listenerMain.hideProgress();
                        layoutDetails.setVisibility(View.VISIBLE);
                    }

                } else {

                    if (listenerMain != null) {
                        listenerMain.hideProgress();
                    }

                    if (code == 300) {
                        Util.message(response.getMessage());
                        Util.errorMessage(getContext(), 2);
                    }
                    if (code == 301) {
                        Util.message(response.getMessage());
                        Util.errorMessage(getContext(), 2);
                    }

                }
            }
        };

        DetailsRequest request = new DetailsRequest(Constants.JSON_SOLICITUDE_DETAIL,proyectList.getIdProyect(),userLogin.getToken(),userLogin.getTokenRefresh());

        spiceProjectDetails.executeRequest(request);

        if (listenerMain != null) {
            listenerMain.showProgress();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (spiceProjectDetails!=null){
            spiceProjectDetails.clearCacheRequest();
        }

    }

    //endregion

    //region Methods

    //region UI clicks

    @Click(R.id.tvEmailScrumMaster)
    protected void sendEmailScrumMaster(){
        if (!Util.testConexion(getContext())) {
        } else {
            Util.sendEmail(getContext(), tvEmailScrumMaster.getText().toString());
        }
    }

    //endregion

    //region Actionbar clicks X
    //endregion

    //region Private X
    //endregion

    //region Protected X
    //endregion

    //region Public X

    //region Getters X
    //endregion

    //region Setters X
    //endregion


    public void setUserLogin(UserLogin userLogin) {
        this.userLogin = userLogin;
    }

    public void setChooser(Project chooser) {
        this.proyectList = chooser;
    }

    public void setInterfaceMain(InterfaceMain listenerSesion) {
        this.listenerMain = listenerSesion;
    }

    //endregion

    //region Super override

    //endregion

    //region Interface implementations

    //region View.OnClickListener

    @Override
    public void onClick(View v) {

    }

    //endregion

    //region LocationListener

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    //endregion

    //endregion

    //endregion

    //region Inner definitions

    //region Interfaces

    //endregion

    //region Classes


    //endregion

    //endregion

}
