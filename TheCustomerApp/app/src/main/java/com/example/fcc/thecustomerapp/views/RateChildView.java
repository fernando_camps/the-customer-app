package com.example.fcc.thecustomerapp.views;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fcc.thecustomerapp.R;
import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.components.GroupItems;
import com.example.fcc.thecustomerapp.event.EventFragments;
import com.example.fcc.thecustomerapp.models.Rate;
import com.example.fcc.thecustomerapp.models.UserLogin;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * Created by fcc on 17/02/16.
 */

@EViewGroup(R.layout.subitems_rate)
public class RateChildView extends FrameLayout {
    int groupPosition;
    int childPosition;
    Rate children;
    UserLogin userLogin;
    ArrayList<GroupItems> grupos;
    Context context;
    boolean notNull;

    @ViewById
    RatingBar ratingBar1;
    @ViewById
    LinearLayout btnConfirmRate;
    @ViewById
    TextView textvw1;
    @ViewById
    TextView nullRate;


    public RateChildView(Context context) {
        super(context);
        this.context = context;
    }

    public void bind(int groupPosition,int childPosition, Rate children,UserLogin userLogin, ArrayList<GroupItems> grupos,boolean notNull){

        if (this.notNull){
            initListeners();
            textvw1.setText(children.getType().toUpperCase() + " " + children.getSprint());
            nullRate.setVisibility(View.GONE);
            ratingBar1.setIsIndicator(false);
            ratingBar1.setFocusable(true);
            btnConfirmRate.setVisibility(View.VISIBLE);
            ratingBar1.setVisibility(View.VISIBLE);
            textvw1.setVisibility(View.VISIBLE);
            ratingBar1.setRating(children.getRate());

            this.childPosition = childPosition;
            this.groupPosition = groupPosition;
            this.children = children;
            this.userLogin = userLogin;
            this.grupos = grupos;

        }else {
            if (nullRate!=null)
                nullRate.setVisibility(View.VISIBLE);
            if (ratingBar1!=null)
                ratingBar1.setVisibility(View.GONE);
            if (textvw1!=null)
                textvw1.setVisibility(View.GONE);
            if (btnConfirmRate!=null)
                btnConfirmRate.setVisibility(View.GONE);
        }

    }

    private void initListeners(){
        ratingBar1.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (fromUser){
                    children.setRate((int) rating);
                }
            }
        });
    }

    @Click(R.id.btnConfirmRate)
    protected void onConfirm() {

        if (children.getRate() != 0) {

            EventFragments event = new EventFragments();
            event.setTag(Constants.TAG_RATED_MSG);
            event.setChildren(children);
            event.setUserLogin(userLogin);
            EventBus.getDefault().post(event);

            event = new EventFragments();
            event.setTag(Constants.TAG_RATED_CONTROL);
            event.setGroupPosition(groupPosition);
            event.setChildPosition(childPosition);
            EventBus.getDefault().post(event);


        } else {
            Toast.makeText(context, "Aun no ha sido asignada la calificacion", Toast.LENGTH_SHORT).show();
        }

    }

}
