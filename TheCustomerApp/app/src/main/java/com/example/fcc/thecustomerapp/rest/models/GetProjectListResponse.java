package com.example.fcc.thecustomerapp.rest.models;

import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.models.Project;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Carlos Briseño on 12/02/2016.
 */
public class GetProjectListResponse {

    @SerializedName(Constants.JSON_CODE)
    private int code;
    @SerializedName(Constants.JSON_MSG)
    private String message;
    @SerializedName(Constants.JSON_SOLICITUDE_PROYECTS)
    private List<Project> projectList;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Project> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<Project> projectList) {
        this.projectList = projectList;
    }
}
