package com.example.fcc.thecustomerapp.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fcc.thecustomerapp.R;
import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.event.EventFragments;
import com.example.fcc.thecustomerapp.models.Project;
import com.example.fcc.thecustomerapp.models.UserLogin;
import com.example.fcc.thecustomerapp.utilities.Util;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * Created by fcc on 17/02/16.
 */

@EViewGroup(R.layout.row_proyects)
public class ProjectView extends FrameLayout {
    Bitmap loadedImage;
    int position;
    Project project;
    UserLogin userLogin;
    ArrayList<Project> projectLists;
    Context context;

    @ViewById
    TextView tvTitleProject;
    @ViewById
    TextView tvDateBegin;
    @ViewById
    TextView tvDateEnd;

    @ViewById
    ImageView btnRate;
    @ViewById
    ImageView ivPhoto;

    public ProjectView(Context context) {
        super(context);
        this.context = context;
    }

    public void bind(Project project,int position, ArrayList<Project> projectLists,UserLogin userLogin){
        tvDateBegin.setText(project.getDateBegin());
        tvDateEnd.setText(project.getDateEnd());
        tvTitleProject.setText(project.getTitleProyect());
        this.projectLists = projectLists;
        this.project = project;
        this.position = position;
        this.userLogin = userLogin;

        //TODO: /* // Falta URL DE LOS SERVICIOS PARA PROBAR
        if (!project.getImageURL().equals("URL")) {
            Util.downloadFile(project.getImageURL(), ivPhoto, loadedImage, context);
        }
    }

   @Click(R.id.btnDetails)
    protected void onDetails() {
    if (projectLists.size()!=0){
    Project chooser = projectLists.get(position);

    EventFragments event = new EventFragments();
    event.setUserLogin(userLogin);
    event.setChooser(chooser);
    event.setTag(Constants.TAG_DETAILS);

    EventBus.getDefault().post(event);
}
    }
    @Click(R.id.btnRate)
    protected void onRate() {
        if (projectLists.size()!=0){

            Project chooser = projectLists.get(position);

            EventFragments event = new EventFragments();
            event.setUserLogin(userLogin);
            event.setChooser(chooser);
            event.setTag(Constants.TAG_RATE);

            EventBus.getDefault().post(event);
        }
    }



}
