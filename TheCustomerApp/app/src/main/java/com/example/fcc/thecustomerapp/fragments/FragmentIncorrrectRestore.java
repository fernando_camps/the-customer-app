package com.example.fcc.thecustomerapp.fragments;

import android.support.v4.app.Fragment;
import android.widget.TextView;

import com.example.fcc.thecustomerapp.R;
import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.event.EventFragments;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

@EFragment(R.layout.activity_fragment_incorrrect_restore)
public class FragmentIncorrrectRestore extends Fragment {
    private String activeUser;

    @ViewById
    TextView tvActiveUserIncorrect;

    @AfterViews
    public void main(){
        tvActiveUserIncorrect.setText(activeUser);
    }

    @Click(R.id.btnReintent)
    protected void buttonReintent(){

        EventFragments event = new EventFragments();
        event.setTag(Constants.TAG_FORGOT_REINTENT);
        EventBus.getDefault().post(event);

        getActivity().onBackPressed();
    }

    public void setActiveUser(String activeUser){
        this.activeUser = activeUser;
    }

}