package com.example.fcc.thecustomerapp.rest.common;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.listener.PendingRequestListener;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 *
 * A simple wrapper of some needed methods to work with RoboSpice, it simplifies the way you create callbacks when you execute RoboSpice
 * requests
 *
 */
public abstract class SpiceHelper<CLASS_RESPONSE> {

    private SpiceManager spiceManager;
    private String cacheKey;
    private boolean isRequestPending;
    private Class responseClass;

    public SpiceHelper(SpiceManager spiceManager, String cacheKey, Class responseClass){
        this.cacheKey = cacheKey;
        this.spiceManager = spiceManager;
        this.responseClass = responseClass;
        isRequestPending = false;
    }

    public void clearCacheRequest(){
        try {
            Future<?> future = spiceManager.removeAllDataFromCache();
            if (future != null){
                future.get();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

//    public boolean isRequestInCache(){
//        try {
//            Future<Boolean> result = spiceManager.isDataInCache(GetStoresResponse.class, cacheKey,DurationInMillis.ONE_HOUR);
//
//            if (result != null && result.get() != null){
//                return (boolean)result.get();
//            }
//
//        } catch (CacheCreationException e) {
//            e.printStackTrace();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
//
//        return false;
//    }

    protected void onFail(SpiceException exception){
        clearCacheRequest(); //Commented in order to avoid the clearing of cache each time a requests is performed
        isRequestPending = false;
    }

    protected void onSuccess(CLASS_RESPONSE response){
        clearCacheRequest(); //Commented in order to avoid the clearing of cache each time a requests is performed
        isRequestPending = false;
    }

    public void executeRequest(SpiceRequest<CLASS_RESPONSE> request){
        isRequestPending = true;
        spiceManager.execute(request, cacheKey, DurationInMillis.ONE_HOUR, new SpiceRequestListener());
    }

    public void executePendingRequest(){
        isRequestPending = true;
        spiceManager.addListenerIfPending(responseClass, cacheKey, new SpicePendingRequestListener());
    }

    public boolean isRequestPending() {
        return isRequestPending;
    }

    private class SpiceRequestListener implements RequestListener<CLASS_RESPONSE> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            onFail(spiceException);
        }

        @Override
        public void onRequestSuccess(CLASS_RESPONSE response) {
            onSuccess(response);
        }
    }

    private class SpicePendingRequestListener implements PendingRequestListener<CLASS_RESPONSE> {

        @Override
        public void onRequestNotFound() {
            if (isRequestPending){
                spiceManager.getFromCache(responseClass, cacheKey, DurationInMillis.ONE_HOUR, new SpiceRequestListener());
            }
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            onFail(spiceException);
        }

        @Override
        public void onRequestSuccess(CLASS_RESPONSE response) {
            onSuccess(response);
        }
    }

}
