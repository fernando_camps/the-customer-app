package com.example.fcc.thecustomerapp.models;

import com.example.fcc.thecustomerapp.components.Constants;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by fcc on 8/01/16.
 */
public class Project {
    private String day;
    private String month;
    private String year;

    @SerializedName(Constants.JSON_PROYECT_NAME)
    private String titleProyect;
    @SerializedName(Constants.JSON_PROYECT_BEGIN_DATE)
    private String dateBegin;
    @SerializedName(Constants.JSON_PROYECT_END_DATE)
    private String dateEnd;
    @SerializedName(Constants.JSON_PROYECT_IMAGE)
    private String imageURL;
    @SerializedName(Constants.JSON_PROYECT_ID)
    private int idProyect;
    @SerializedName(Constants.JSON_COMMENTS)
    private String comments;
    @SerializedName(Constants.JSON_SOLICITUDE_CONTACT)
    private Contact contact;
    @SerializedName(Constants.JSON_SOLICITUDE_TECNOLOGYS)
    private List<Tecnology> projectList;

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public List<Tecnology> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<Tecnology> projectList) {
        this.projectList = projectList;
    }

    public String getTitleProyect() {
        return titleProyect;
    }

    public void setTitleProyect(String titleProyect) {
        this.titleProyect = titleProyect;
    }

    public int getIdProyect() {
        return idProyect;
    }

    public void setIdProyect(int idProyect) {
        this.idProyect = idProyect;
    }

    public String getImageURL() {
        return imageURL;

    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(String dateBegin) {
        this.dateBegin = dateBegin;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getFormmatedDate(String date){
        day = date.substring(8, 10);
        month = date.substring(5, 7);
        year = date.substring(0, 4);
        date = (day + Constants.DETAILS_DATE_SEPARATOR + month + Constants.DETAILS_DATE_SEPARATOR + year);

        return date;
    }

}


