package com.example.fcc.thecustomerapp.rest.requests;

import com.example.fcc.thecustomerapp.rest.models.GetProjectDetailsRequest;
import com.example.fcc.thecustomerapp.rest.models.GetProjectDetailsResponse;
import com.example.fcc.thecustomerapp.rest.webservices.DetailsWebService;


/**
 * Created by fcc on 16/02/16.
 */
public class DetailsRequest extends BaseRequest<GetProjectDetailsResponse, DetailsWebService>{

    private GetProjectDetailsRequest detailsRequest;

    public DetailsRequest(String request, int idProject, String token, String tokenRefresh){
        super(GetProjectDetailsResponse.class, DetailsWebService.class);

        detailsRequest = new GetProjectDetailsRequest();

        detailsRequest.setRequest(request);
        detailsRequest.setIdProject(idProject);
        detailsRequest.setToken(token);
        detailsRequest.setTokenRefresh(tokenRefresh);
    }

    @Override
    public GetProjectDetailsResponse loadDataFromNetwork() throws Exception {
        return getService().getDetails(detailsRequest);
    }
}
