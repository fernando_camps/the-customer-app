package com.example.fcc.thecustomerapp.rest.requests;

import com.example.fcc.thecustomerapp.rest.models.GetProjectForgotRequest;
import com.example.fcc.thecustomerapp.rest.models.GetProjectForgotResponse;
import com.example.fcc.thecustomerapp.rest.models.GetProjectLoginRequest;
import com.example.fcc.thecustomerapp.rest.models.GetProjectLoginResponse;
import com.example.fcc.thecustomerapp.rest.webservices.ForgotWebService;
import com.example.fcc.thecustomerapp.rest.webservices.LoginWebService;

/**
 * Created by fcc on 16/02/16.
 */
public class ForgotRequest  extends BaseRequest<GetProjectForgotResponse, ForgotWebService>{

    private GetProjectForgotRequest forgotRequest;

    public ForgotRequest(String contactEmail, String request){
        super(GetProjectForgotResponse.class, ForgotWebService.class);

        forgotRequest = new GetProjectForgotRequest();

        forgotRequest.setContactEmail(contactEmail);
        forgotRequest.setRequest(request);

    }

    @Override
    public GetProjectForgotResponse loadDataFromNetwork() throws Exception {
        return getService().getForgot(forgotRequest);
    }
}
