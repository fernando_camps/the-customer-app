package com.example.fcc.thecustomerapp.fragments;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.fcc.thecustomerapp.R;
import com.example.fcc.thecustomerapp.adapters.AdapterProyect;
import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.components.InterfaceMain;
import com.example.fcc.thecustomerapp.event.EventFragments;
import com.example.fcc.thecustomerapp.models.Project;
import com.example.fcc.thecustomerapp.models.UserLogin;
import com.example.fcc.thecustomerapp.rest.common.SpiceHelper;
import com.example.fcc.thecustomerapp.rest.models.GetProjectListResponse;
import com.example.fcc.thecustomerapp.rest.requests.ProjectListRequest;
import com.example.fcc.thecustomerapp.utilities.Util;
import com.octo.android.robospice.persistence.exception.SpiceException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

@EFragment(R.layout.activity_fragment_list_proyects)
public class FragmentListProyects extends BaseRestFragment {

    //region Variables

    //region Constants X
    //endregion

    //region Primitives
    private boolean refresh;
    //endregion

    //region Objects
    private  Toast toast;
    private AdapterProyect adapter;
    private ArrayList<Project> projectLists;
    private UserLogin userLogin;
    private InterfaceMain listenerMain;
    private SpiceHelper<GetProjectListResponse> spiceProjectList;
    //endregion

    //region Views
    @ViewById
    ListView lvProjects;
    @ViewById
    SwipeRefreshLayout swipeRefreshLayout;
    //endregion

    //endregion

    //region Component life cycle
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.activity_fragment_list_proyects, container, false);
    }

    @AfterViews
    public void main(){

        setHasOptionsMenu(true);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        initListeners();

        spiceProjectList = new SpiceHelper<GetProjectListResponse>(getSpiceManager(), "CACHE_LIST", GetProjectListResponse.class) {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                Util.message(Constants.ERROR_ROBOSPICE_PETICION);
                exception.printStackTrace();

                if (listenerMain != null) {
                    listenerMain.hideProgress();
                }

                Util.errorMessage(getContext(), 2);
            }

            @Override
            protected void onSuccess(GetProjectListResponse response) {
                super.onSuccess(response);

                int code = response.getCode();

                if (code == 200) {

                    projectLists = new ArrayList<>();

                    for (int i = 0; i < response.getProjectList().size(); i++) {

                        Project project = new Project();
                        project = response.getProjectList().get(i);

                        if (project.getDateBegin().length() != 0) {
                            project.setDateBegin(project.getFormmatedDate(project.getDateBegin()));
                        }

                        if (project.getDateEnd().length() != 0) {
                            project.setDateEnd(project.getFormmatedDate(project.getDateEnd()));
                        }
                        projectLists.add(project);
                    }

                    swipeRefreshLayout.setRefreshing(false);
                    rowAdapter();
                    refresh = false;

                    if (listenerMain != null) {
                        listenerMain.hideProgress();
                        lvProjects.setEnabled(true);
                    }




                } else {

                    if (code == 300) {
                        Util.message(response.getMessage());
                        Util.errorMessage(getContext(), 2);
                    }
                    if (code == 301) {
                        Util.message(response.getMessage());
                        Util.errorMessage(getContext(), 2);
                    }

                }

                if (listenerMain != null) {
                    listenerMain.hideProgress();
                    lvProjects.setEnabled(true);
                }
            }
        };

        ProjectListRequest request = new ProjectListRequest(Constants.JSON_SOLICITUDE_LIST, String.valueOf(userLogin.getIdUser()));

        spiceProjectList.executeRequest(request);

        if (listenerMain!=null){
            listenerMain.showProgress();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (spiceProjectList!=null){
            spiceProjectList.clearCacheRequest();
        }
    }

    //onAttach, onResume, onCreate, onPause, onStop, onDestroy, activityForResult

    //endregion

    //region Methods

    //region UI clicks X
    //endregion

    //region Actionbar clicks X
    //endregion

    //region Private
    private void initListeners() {
        lvProjects.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Project chooser = (Project) parent.getItemAtPosition(position);

                if (!Util.testConexion(getContext())) {
                } else {

                    if (!swipeRefreshLayout.isRefreshing()){
                        listenerMain.showProgress();

                        EventFragments event = new EventFragments();
                        event.setUserLogin(userLogin);
                        event.setChooser(chooser);
                        event.setTag(Constants.TAG_DETAILS);

                        EventBus.getDefault().post(event);
                    }
                }
                /*fragmentRate.setProyectList(elegido);*/

               /* CharSequence texto = "Seleccionado: " + elegido.getTitleProyect();
                Toast toast = Toast.makeText(getActivity(), texto, Toast.LENGTH_LONG);
                toast.show();*/
            }
        });

        //TODO: VALIDAR REFRESH
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!Util.testConexion(getContext())) {
                } else {
                    if (!refresh && adapter!=null){
                        refresh = true;
                        lvProjects.setEnabled(false);

                        projectLists.clear();

                        ProjectListRequest request = new ProjectListRequest(Constants.JSON_SOLICITUDE_LIST, String.valueOf(userLogin.getIdUser()));
                        spiceProjectList.executeRequest(request);

                        if (toast!=null){
                            toast.cancel();
                        }
                        toast = Toast.makeText(getContext(), getContext().getString(R.string.list_refresh), Toast.LENGTH_SHORT);
                        toast.show();
                    }

                }
            }
        });

    }

    private void rowAdapter() {
        if (adapter==null){
            adapter = new AdapterProyect(projectLists, getActivity(), userLogin);
            lvProjects.setAdapter(adapter);
        }else {
            adapter.setObjects(projectLists, getActivity(), userLogin);
            adapter.notifyDataSetChanged();
        }
    }
    //endregion

    //region Protected X
    //endregion

    //region Public

    public void setUserLogin(UserLogin userLogin) {
        this.userLogin = userLogin;

    }

    public void setInterfaceMain(InterfaceMain listenerMain) {
        this.listenerMain = listenerMain;
    }

    //region Getters X
    //endregion

    //region Setters X
    //endregion

    //endregion

    //region Super override
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    inflater.inflate(R.menu.menu_main, menu);
    super.onCreateOptionsMenu(menu, inflater);
    }
    //endregion

    //region Interface implementations X

//region example-onClickListener
//endregion

//region example-onLocationListener
//endregion

//region example-onCustomActionListener
//endregion

//endregion

    //endregion

    //region Inner definitions X

//region Interfaces X

//endregion

//region Classes X
//endregion

//endregion

}

