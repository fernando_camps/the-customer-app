package com.example.fcc.thecustomerapp.components;

/**
 * Created by fcc on 8/02/16.
 */
public interface InterfaceMain {

    public void showProgress();

    public void hideProgress();
}
