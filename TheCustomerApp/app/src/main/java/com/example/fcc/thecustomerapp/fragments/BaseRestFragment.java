package com.example.fcc.thecustomerapp.fragments;

import android.support.v4.app.Fragment;

import com.example.fcc.thecustomerapp.rest.common.RestService;
import com.octo.android.robospice.SpiceManager;


public class BaseRestFragment extends Fragment{

    private SpiceManager spiceManager = new SpiceManager(RestService.class);

    @Override
    public void onStart() {
        spiceManager.start(getActivity().getApplicationContext());
        super.onStart();
    }

    @Override
    public void onStop() {
        if (spiceManager.isStarted()) {
            spiceManager.shouldStop();
        }
        super.onStop();
    }

    protected SpiceManager getSpiceManager() {
        return spiceManager;
    }

}
