package com.example.fcc.thecustomerapp.rest.models;

import com.example.fcc.thecustomerapp.components.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fcc on 15/02/16.
 */
public class GetProjectLoginRequest {

    @SerializedName(Constants.JSON_LOGIN)
    private String userLogin;
    @SerializedName(Constants.JSON_PASS)
    private String userPass;

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }
}
