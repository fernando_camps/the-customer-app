package com.example.fcc.thecustomerapp.fragments;

import android.support.v4.app.Fragment;
import android.widget.TextView;

import com.example.fcc.thecustomerapp.R;
import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.event.EventFragments;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

@EFragment(R.layout.activity_fragment_correct_restore)
public class FragmentCorrectRestore extends Fragment {
    private String activeUserForgot;

    @ViewById
    TextView tvActiveUserCorrect;

    @AfterViews
    public void main() {
        tvActiveUserCorrect.setText(activeUserForgot);
    }

    public void setActiveUserForgot(String activeUserForgot) {
        this.activeUserForgot = activeUserForgot;
    }

    @Click(R.id.btnOkCorrect)
    protected void buttonOk() {

        EventFragments event = new EventFragments();
        event.setTag(Constants.TAG_FORGOT_ACCEPT);
        EventBus.getDefault().post(event);

    }
}
