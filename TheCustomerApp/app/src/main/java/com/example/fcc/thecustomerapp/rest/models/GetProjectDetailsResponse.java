package com.example.fcc.thecustomerapp.rest.models;

import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.models.Contact;
import com.example.fcc.thecustomerapp.models.Project;
import com.example.fcc.thecustomerapp.models.Tecnology;
import com.example.fcc.thecustomerapp.models.UserLogin;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by fcc on 16/02/16.
 */
public class GetProjectDetailsResponse {

    @SerializedName(Constants.JSON_CODE)
    private int code;
    @SerializedName(Constants.JSON_MSG)
    private String message;
    @SerializedName(Constants.JSON_SOLICITUDE_PROYECT)
    private Project projectDetails;

    public Project getProject() {
        return projectDetails;
    }

    public void setProject(Project project) {
        this.projectDetails = project;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
