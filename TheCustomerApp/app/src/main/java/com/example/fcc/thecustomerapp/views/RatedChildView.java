package com.example.fcc.thecustomerapp.views;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fcc.thecustomerapp.R;
import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.components.GroupItems;
import com.example.fcc.thecustomerapp.models.Rate;
import com.example.fcc.thecustomerapp.models.UserLogin;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

/**
 * Created by fcc on 17/02/16.
 */

@EViewGroup(R.layout.subitems_rated)
public class RatedChildView extends FrameLayout {
    int groupPosition;
    int childPosition;
    Rate children;
    UserLogin userLogin;
    ArrayList<GroupItems> grupos;
    Context context;

    @ViewById
    RatingBar ratingBar2;
    @ViewById
    TextView textView2;
    @ViewById
    TextView nullRated;

    public RatedChildView(Context context) {
        super(context);
        this.context = context;
    }

    public void bind(int groupPosition,int childPosition, Rate children,UserLogin userLogin, ArrayList<GroupItems> grupos){
            nullRated.setVisibility(View.GONE);
            ratingBar2.setIsIndicator(true);
            ratingBar2.setFocusable(false);
            ratingBar2.setRating(children.getRate());
            textView2.setText(children.getType().toUpperCase() + " " + children.getSprint());

        this.childPosition = childPosition;
        this.groupPosition = groupPosition;
        this.children = children;
        this.userLogin = userLogin;
        this.grupos = grupos;
    }

}
