package com.example.fcc.thecustomerapp.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.fcc.thecustomerapp.models.Project;
import com.example.fcc.thecustomerapp.models.UserLogin;
import com.example.fcc.thecustomerapp.views.ProjectView;
import com.example.fcc.thecustomerapp.views.ProjectView_;

import java.util.ArrayList;

public class AdapterProyect extends BaseAdapter {
    private ArrayList<Project> proyectLists;
    private Context context;
    private UserLogin userLogin;

    public AdapterProyect(ArrayList<Project> proyectLists, Context context, UserLogin userLogin) {
        this.proyectLists = proyectLists;
        this.context = context;
        this.userLogin = userLogin;
    }

    @Override
    public int getCount() {
        return proyectLists.size();
    }

    @Override
    public Project getItem(int position) {
        return proyectLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Project project = getItem(position);
        ProjectView projectView;

        if(convertView == null){
            convertView = ProjectView_.build(context);
        }

        projectView = (ProjectView) convertView;
        projectView.bind(project,position,proyectLists,userLogin);

        return convertView;
    }

    public void setObjects(ArrayList<Project> proyectLists, Context context, UserLogin userLogin) {
        this.proyectLists = proyectLists;
        this.context = context;
        this.userLogin = userLogin;
    }

}