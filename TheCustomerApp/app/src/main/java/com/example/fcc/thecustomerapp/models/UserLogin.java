package com.example.fcc.thecustomerapp.models;

import com.example.fcc.thecustomerapp.components.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fcc on 13/01/16.
 */
public class UserLogin {

    @SerializedName(Constants.JSON_ID_USER)
    private int idUser;
    @SerializedName(Constants.JSON_ID_CLIENT)
    private int idClient;
    @SerializedName(Constants.JSON_COMPLETE_NAME_USER)
    private String completeName;
    @SerializedName(Constants.JSON_TOKEN)
    private String token;
    @SerializedName(Constants.JSON_REFRESH_TOKEN)
    private String tokenRefresh;

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenRefresh() {
        return tokenRefresh;
    }

    public void setTokenRefresh(String tokenRefresh) {
        this.tokenRefresh = tokenRefresh;
    }
}
