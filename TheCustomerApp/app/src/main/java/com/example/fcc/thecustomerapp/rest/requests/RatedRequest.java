package com.example.fcc.thecustomerapp.rest.requests;

import com.example.fcc.thecustomerapp.rest.models.GetProjectRateRequest;
import com.example.fcc.thecustomerapp.rest.models.GetProjectRatedResponse;
import com.example.fcc.thecustomerapp.rest.webservices.RatedWebService;

/**
 * Created by fcc on 16/02/16.
 */
public class RatedRequest extends BaseRequest<GetProjectRatedResponse, RatedWebService>{

    private GetProjectRateRequest ratedRequest;

    public RatedRequest(String request, int idProject, String token, String tokenRefresh){
        super(GetProjectRatedResponse.class, RatedWebService.class);

        ratedRequest = new GetProjectRateRequest();

        ratedRequest.setRequest(request);
        ratedRequest.setIdProject(idProject);
        ratedRequest.setToken(token);
        ratedRequest.setTokenRefresh(tokenRefresh);
    }

    @Override
    public GetProjectRatedResponse loadDataFromNetwork() throws Exception {
        return getService().getRated(ratedRequest);
    }
}
