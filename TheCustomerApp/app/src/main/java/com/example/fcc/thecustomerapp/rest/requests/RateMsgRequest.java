package com.example.fcc.thecustomerapp.rest.requests;

import com.example.fcc.thecustomerapp.rest.models.GetProjectRateRequest;
import com.example.fcc.thecustomerapp.rest.models.GetProjectRateResponse;
import com.example.fcc.thecustomerapp.rest.models.GetRateMsgRequest;
import com.example.fcc.thecustomerapp.rest.models.GetRateMsgResponse;
import com.example.fcc.thecustomerapp.rest.webservices.RateMsgWebService;
import com.example.fcc.thecustomerapp.rest.webservices.RateWebService;

/**
 * Created by fcc on 16/02/16.
 */
public class RateMsgRequest extends BaseRequest<GetRateMsgResponse, RateMsgWebService>{

    private GetRateMsgRequest rateMsgRequest;

    public RateMsgRequest(String request, String idProject,int rate, String token, String tokenRefresh){
        super(GetRateMsgResponse.class, RateMsgWebService.class);

        rateMsgRequest = new GetRateMsgRequest();

        rateMsgRequest.setRequest(request);
        rateMsgRequest.setIdProject(idProject);
        rateMsgRequest.setRate(rate);
        rateMsgRequest.setToken(token);
        rateMsgRequest.setTokenRefresh(tokenRefresh);
    }

    @Override
    public GetRateMsgResponse loadDataFromNetwork() throws Exception {
        return getService().getRateMsg(rateMsgRequest);
    }
}
