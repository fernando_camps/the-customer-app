package com.example.fcc.thecustomerapp.fragments;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.fcc.thecustomerapp.R;
import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.components.InterfaceMain;
import com.example.fcc.thecustomerapp.event.EventFragments;
import com.example.fcc.thecustomerapp.rest.common.SpiceHelper;
import com.example.fcc.thecustomerapp.rest.models.GetProjectForgotResponse;
import com.example.fcc.thecustomerapp.rest.requests.ForgotRequest;
import com.example.fcc.thecustomerapp.utilities.Util;
import com.octo.android.robospice.persistence.exception.SpiceException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

@EFragment(R.layout.activity_fragment_forgot_pass)
public class FragmentForgotPass extends BaseRestFragment{

    //region Variables

    //region Constants X
    //endregion

    //region Primitives X
    //endregion

    //region Objects
    private InterfaceMain listenerMain;
    private String activeUserForgot;
    private SpiceHelper<GetProjectForgotResponse> spiceProjectForgot;
    //endregion

    //region Views
    @ViewById
    EditText editForgotAccount;
    @ViewById
    Button btnAcceptRestore;
    @ViewById
    ImageButton btnDeleteForgot;
    //endregion

    //endregion

    //region Component life cycle
    @AfterViews
    public void main(){

        btnDeleteForgot.setVisibility(View.INVISIBLE);
        btnAcceptRestore.setClickable(false);
        editForgotAccount.requestFocus();

        spiceProjectForgot = new SpiceHelper<GetProjectForgotResponse>(getSpiceManager(), "CACHE_LIST", GetProjectForgotResponse.class) {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                Util.message(Constants.ERROR_ROBOSPICE_PETICION);
                exception.printStackTrace();

                if (listenerMain != null) {
                    listenerMain.hideProgress();
                }

                Util.errorMessage(getContext(), 2);
            }

            @Override
            protected void onSuccess(GetProjectForgotResponse response) {
                super.onSuccess(response);

                EventFragments event = new EventFragments();
                event.setActiveUserForgot(activeUserForgot);

                int code = response.getCode();

                if (code == 200) {

                    Util.message(response.getMessage());

                    event.setTag(Constants.TAG_CORRECT_RESTORE);
                    EventBus.getDefault().post(event);

                    if (listenerMain != null) {
                        listenerMain.hideProgress();
                    }

                } else {

                    event.setTag(Constants.TAG_INCORRECT_RESTORE);
                    EventBus.getDefault().post(event);

                    if (code == 300) {
                        Util.message(response.getMessage());
                        Util.errorMessage(getContext(), code);
                    }

                    if (code == 301) {
                        Util.message(response.getMessage());
                        Util.errorMessage(getContext(), code);
                    }
                    if (code == 302) {
                        Util.message(response.getMessage());
                        Util.errorMessage(getContext(), code);
                    }
                    if (code == 303) {
                        Util.message(response.getMessage());
                        ;
                        Util.errorMessage(getContext(), code);
                    }

                    if (listenerMain != null) {
                        listenerMain.hideProgress();
                    }

                }
            }

        };

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (spiceProjectForgot!=null){
            spiceProjectForgot.clearCacheRequest();
        }

    }

    //onAttach, onResume, onCreate, onPause, onStop, onDestroy, activityForResult

    //endregion

    //region Methods X

    //region UI clicks
    @Click(R.id.btnAcceptRestore)
    protected void aceptRestore(){
        if (!Util.testConexion(getContext())) {
        } else {

            ForgotRequest request = new ForgotRequest(editForgotAccount.getText().toString(), Constants.JSON_SOLICITUDE_RECOVER);
            setActiveUserForgot(editForgotAccount.getText().toString());

            spiceProjectForgot.executeRequest(request);
            Util.hideSoftKeyboard(getActivity());

            if (listenerMain != null) {
                listenerMain.showProgress();
            }

        }
    }

    @Click(R.id.btnCancelRestore)
    protected void cancelRestore(){
        EventFragments event = new EventFragments();
        event.setTag(Constants.TAG_FORGOT_CANCEL);
        EventBus.getDefault().post(event);
    }

    @Click(R.id.btnDeleteForgot)
    protected void deleteText(){
        editForgotAccount.setText("");
    }
    //endregion

    //region UI events
    @TextChange(R.id.editForgotAccount)
    void onTextChangesOnSomeTextViews(TextView tv, CharSequence text) {
        if (text.length() == 0){
            btnDeleteForgot.setVisibility(View.INVISIBLE);
            btnAcceptRestore.setClickable(false);
        }else{
            btnDeleteForgot.setVisibility(View.VISIBLE);
            btnAcceptRestore.setClickable(true);
        }
    }
    //endregion

    //region Actionbar clicks X
    //endregion

    //region Private
    private void setActiveUserForgot(String activeUserForgot) {
        this.activeUserForgot = activeUserForgot;
    }
    //endregion

    //region Protected X
    //endregion

    //region Public
    public void setInterfaceMain(InterfaceMain listenerMain){
        this.listenerMain = listenerMain;
    }

    //region Getters X
    //endregion

    //region Setters X
    //endregion

    //endregion

    //region Super override X
    //endregion

    //region Interface implementations X

    //region example-onClickListener
    //endregion

    //region example-onLocationListener
    //endregion

    //region example-onCustomActionListener
    //endregion

    //endregion

    //endregion

    //region Inner definitions X

    //region Interfaces X

    //endregion

    //region Classes X
    //endregion

    //endregion

}
