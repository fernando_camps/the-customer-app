package com.example.fcc.thecustomerapp.models;

import com.example.fcc.thecustomerapp.components.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fcc on 14/01/16.
 */
public class Contact {

    @SerializedName(Constants.JSON_CONTACT_NAME)
    private String name;
    @SerializedName(Constants.JSON_CONTACT_ROLE)
    private String role;
    @SerializedName(Constants.JSON_CONTACT_EMAIL)
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
