package com.example.fcc.thecustomerapp.rest.requests;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.octo.android.robospice.retry.DefaultRetryPolicy;
import com.octo.android.robospice.retry.RetryPolicy;

/**
 * Base class in order to perform requests using the Retrofit plugin of RoboSpice
 */
public abstract class BaseRequest<T, R> extends RetrofitSpiceRequest<T, R> {

    public BaseRequest(Class clazz, Class retrofitedInterfaceClass) {
        super(clazz, retrofitedInterfaceClass);
    }

    /**
     * We override this method in order to define a new retry policy due the default one does 3 attempts of connection on every request.
     * With this parameter the request will do only one attempt per request.
     * @return
     */
    @Override
    public RetryPolicy getRetryPolicy() {
        return new DefaultRetryPolicy(0,0,1);
    }
}
