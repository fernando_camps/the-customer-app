package com.example.fcc.thecustomerapp.rest.webservices;

import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.rest.models.GetProjectForgotRequest;
import com.example.fcc.thecustomerapp.rest.models.GetProjectForgotResponse;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by fcc on 16/02/16.
 */
public interface ForgotWebService {
    @POST(Constants.URL_RESTORE)
    GetProjectForgotResponse getForgot(@Body GetProjectForgotRequest body);
}
