package com.example.fcc.thecustomerapp.fragments;

import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fcc.thecustomerapp.R;
import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.components.InterfaceMain;
import com.example.fcc.thecustomerapp.event.EventFragments;
import com.example.fcc.thecustomerapp.models.UserLogin;
import com.example.fcc.thecustomerapp.rest.common.SpiceHelper;
import com.example.fcc.thecustomerapp.rest.models.GetProjectLoginResponse;
import com.example.fcc.thecustomerapp.rest.requests.LoginRequest;
import com.example.fcc.thecustomerapp.utilities.Util;
import com.octo.android.robospice.persistence.exception.SpiceException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@EFragment(R.layout.activity_login)
public class FragmentLogin extends BaseRestFragment {

    //region Variables

    //region Constants X
    //endregion

    //region Primitives X
    //endregion

    //region Objects
    private InterfaceMain listenerMain;
    private SpiceHelper<GetProjectLoginResponse> spiceLogin;
    //endregion

    //region Views
    @ViewById
    TextView tvInitSession;
    @ViewById
    TextView tvForgotPass;
    @ViewById
    EditText edAccount;
    @ViewById
    EditText edPass;
    @ViewById
    ImageButton btnDelete;
    @ViewById
    LinearLayout layoutMsgError;
    @ViewById
    ImageView ivCloseError;
    //endregion

    //endregion

    //region Component life cycle
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @AfterViews
    public void main(){
        layoutMsgError.setVisibility(View.GONE);
        btnDelete.setVisibility(View.INVISIBLE);
        tvInitSession.setClickable(false);

        SpannableString content = new SpannableString(Constants.MESSAGE_FORGOT);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvForgotPass.setText(content);

        spiceLogin = new SpiceHelper<GetProjectLoginResponse>(getSpiceManager(), "CACHE_LOGIN", GetProjectLoginResponse.class) {

            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);

                Util.message(Constants.ERROR_ROBOSPICE_PETICION);
                exception.printStackTrace();

                if (listenerMain != null) {
                    listenerMain.hideProgress();
                }

                Util.errorMessage(getContext(), 2);
            }

            @Override
            protected void onSuccess(GetProjectLoginResponse response) {
                super.onSuccess(response);

                int code = response.getCode();

                if (code == 200) {

                    UserLogin userLogin = new UserLogin();
                    userLogin = response.getUserLogin();
                    userLogin.setEmail((edAccount.getText().toString()));

                    EventFragments event = new EventFragments();
                    event.setTag(Constants.TAG_LIST);
                    event.setUserLogin(userLogin);
                    EventBus.getDefault().post(event);

                    if (listenerMain != null) {
                        listenerMain.hideProgress();
                    }

                }else{
                    if (code == 301) {
                        Util.message(response.getMessage());
                    }
                    if (code == 302) {
                        Util.message(response.getMessage());
                    }
                    layoutMsgError.setVisibility(View.VISIBLE);
                    tvForgotPass.setVisibility(View.GONE);

                    if (listenerMain != null) {
                        listenerMain.hideProgress();
                    }
                }
            }

        };

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (spiceLogin!=null){
            spiceLogin.clearCacheRequest();
        }

    }
    //onAttach, onResume, onCreate, onPause, onStop, onDestroy, activityForResult

    //endregion

    //region Methods X

    //region UI clicks
    @Click(R.id.btnDelete)
    protected void clearFields() {
        cleanLogin();
        showTvForgot();
    }

    @Click(R.id.tvInitSession)
    protected void initSesion() {
        showTvForgot();

        if (!Util.testConexion(getContext())) {

        } else {
            if (listenerMain!=null){
                listenerMain.showProgress();
            }
            if (edPass.getText().length() >= 6) {

                if (!spiceLogin.isRequestPending()){

                    LoginRequest request = new LoginRequest(edAccount.getText().toString(),md5(edPass.getText().toString()));

                    spiceLogin.executeRequest(request);
                    Util.hideSoftKeyboard(getActivity());
                }


            } else {
                Toast.makeText(getContext(),getContext().getString(R.string.error_min_password),Toast.LENGTH_SHORT).show();
                if (listenerMain!=null){
                    listenerMain.hideProgress();
                }
            }

        }
    }

    @Click(R.id.ivCloseError)
    protected void closeError() {
        showTvForgot();
    }

    @Click(R.id.tvForgotPass)
    protected void showRestore() {
        if (!Util.testConexion(getContext())) {
            Toast.makeText(getActivity(),
                    Constants.MSG_TEST_CONEXION, Toast.LENGTH_SHORT)
                    .show();

        } else {
            EventFragments event = new EventFragments();
            event.setTag(Constants.TAG_FORGOT);
            EventBus.getDefault().post(event);

            edAccount.setFocusable(false);
            edAccount.setFocusableInTouchMode(false);
            edPass.setFocusable(false);
            edPass.setFocusableInTouchMode(false);

        }
    }
    //endregion

    //region UI events
    @TextChange(R.id.edAccount)
    void onTextChangesOnSomeTextViews(TextView tv, CharSequence text) {
        if (text.length() == 0){
            btnDelete.setVisibility(View.INVISIBLE);
            tvInitSession.setClickable(false);
        }else{
            btnDelete.setVisibility(View.VISIBLE);
            tvInitSession.setClickable(true);
        }
    }
    //endregion

    //region Actionbar clicks X
    //endregion

    //region Private

    private void showTvForgot(){
        layoutMsgError.setVisibility(View.GONE);
        tvForgotPass.setVisibility(View.VISIBLE);
    }
    //endregion

    //region Protected X
    //endregion

    //region Public
    public static String md5(String string) {
        byte[] hash;

        try {
            hash = MessageDigest.getInstance(Constants.MD5_TITTLE).digest(string.getBytes(Constants.MD5_UTF));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(Constants.MD5_NO_SUCH_ALGORITHM_EXCEPTION, e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(Constants.MD5_UNSUPPORTED_ENCODING_EXCEPTION, e);
        }

        StringBuilder hex = new StringBuilder(hash.length * 2);

        for (byte b : hash) {
            int i = (b & 0xFF);
            if (i < 0x10) hex.append('0');
            hex.append(Integer.toHexString(i));
        }

        return hex.toString();
    }

    public void cleanLogin() {
        edAccount.setText("");
        edPass.setText("");
    }

    public void setInterfaceMain(InterfaceMain listenerMain) {
        this.listenerMain = listenerMain;
    }

    public void activateLogin() {
        edAccount.setFocusable(true);
        edAccount.setFocusableInTouchMode(true);
        edPass.setFocusable(true);
        edPass.setFocusableInTouchMode(true);
    }

    //region Getters X
    //endregion

    //region Setters X
    //endregion

    //endregion

    //region Super override X
    //endregion

    //region Interface implementations X

    //region example-onClickListener
    //endregion

    //region example-onLocationListener
    //endregion

    //region example-onCustomActionListener
    //endregion

    //endregion

    //endregion

    //region Inner definitions X

    //region Interfaces X

    //endregion

    //region Classes X
    //endregion

    //endregion

}