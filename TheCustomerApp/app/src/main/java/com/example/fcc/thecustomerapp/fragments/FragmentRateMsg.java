package com.example.fcc.thecustomerapp.fragments;

import com.example.fcc.thecustomerapp.R;
import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.components.InterfaceMain;
import com.example.fcc.thecustomerapp.rest.common.SpiceHelper;
import com.example.fcc.thecustomerapp.rest.models.GetRateMsgResponse;
import com.example.fcc.thecustomerapp.rest.requests.RateMsgRequest;
import com.example.fcc.thecustomerapp.utilities.Util;
import com.octo.android.robospice.persistence.exception.SpiceException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

@EFragment(R.layout.activity_fragment_rate_msg)
public class FragmentRateMsg extends BaseRestFragment{
    private int rate;
    private String idProject;
    private String token;
    private String refresh_token;
    private InterfaceMain listenerMain;
    private SpiceHelper<GetRateMsgResponse> spiceRateMsg;

    @AfterViews
    public void main(){

        spiceRateMsg = new SpiceHelper<GetRateMsgResponse>(getSpiceManager(), "CACHE_LIST", GetRateMsgResponse.class) {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                Util.message(Constants.ERROR_ROBOSPICE_PETICION);
                exception.printStackTrace();

                if (listenerMain != null) {
                    listenerMain.hideProgress();
                }

                Util.errorMessage(getContext(), 2);
            }

            @Override
            protected void onSuccess(GetRateMsgResponse response) {
                super.onSuccess(response);

                int code = response.getCode();

                if (code == 200) {

                    if (listenerMain != null) {
                        listenerMain.hideProgress();
                    }

                    boolean result = response.isResult();
                    Util.message(response.getMessage() + "" + result);

                } else {

                    if (listenerMain != null) {
                        listenerMain.hideProgress();
                    }

                    if (code == 300) {
                        Util.message(response.getMessage());
                        Util.errorMessage(getContext(), 2);
                    }
                    if (code == 301) {
                        Util.message(response.getMessage());
                        Util.errorMessage(getContext(), 2);
                    }
                }
            }
        };

        RateMsgRequest request = new RateMsgRequest(Constants.JSON_SOLICITUDE_RATED_MSG,idProject,rate,token,refresh_token);

        /*spiceRateMsg.executeRequest(request);*/

    }

    @Click(R.id.btnAcceptRateMsg)
    protected void acceptRate(){
        getActivity().onBackPressed();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (spiceRateMsg!=null){
            spiceRateMsg.clearCacheRequest();
        }
    }

    public void setCorrectRate(int rate, String id, String token, String token_refresh) {
        this.rate = rate;
        this.idProject = id;
        this.token = token;
        this.refresh_token = token_refresh;
    }

    public void setInterfaceMain(InterfaceMain listenerMain) {
        this.listenerMain = listenerMain;
    }
}
