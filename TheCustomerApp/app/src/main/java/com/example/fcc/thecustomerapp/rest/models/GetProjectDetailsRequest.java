package com.example.fcc.thecustomerapp.rest.models;

import com.example.fcc.thecustomerapp.components.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fcc on 15/02/16.
 */
public class GetProjectDetailsRequest {

    @SerializedName(Constants.JSON_SOLICITUDE)
    private String request;
    @SerializedName(Constants.JSON_PROYECT_ID)
    private int idProject;
    @SerializedName(Constants.JSON_TOKEN)
    private String token;
    @SerializedName(Constants.JSON_REFRESH_TOKEN)
    private String tokenRefresh;

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public int getIdProject() {
        return idProject;
    }

    public void setIdProject(int idProject) {
        this.idProject = idProject;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenRefresh() {
        return tokenRefresh;
    }

    public void setTokenRefresh(String tokenRefresh) {
        this.tokenRefresh = tokenRefresh;
    }
}
