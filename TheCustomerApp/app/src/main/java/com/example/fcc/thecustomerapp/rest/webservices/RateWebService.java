package com.example.fcc.thecustomerapp.rest.webservices;

import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.rest.models.GetProjectRateRequest;
import com.example.fcc.thecustomerapp.rest.models.GetProjectRateResponse;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by fcc on 16/02/16.
 */
public interface RateWebService {
    @POST(Constants.URL_RATE)
    GetProjectRateResponse getRate(@Body GetProjectRateRequest body);
}
