package com.example.fcc.thecustomerapp.event;

import com.example.fcc.thecustomerapp.models.Project;
import com.example.fcc.thecustomerapp.models.Rate;
import com.example.fcc.thecustomerapp.models.UserLogin;

/**
 * Created by fcc on 22/02/16.
 */
public class EventFragments {
    private int groupPosition;
    private int childPosition;
    private String tag;
    private String activeUserForgot;
    private UserLogin userLogin;
    private Project chooser;
    private Rate children;

    public int getGroupPosition() {
        return groupPosition;
    }

    public void setGroupPosition(int groupPosition) {
        this.groupPosition = groupPosition;
    }

    public int getChildPosition() {
        return childPosition;
    }

    public void setChildPosition(int childPosition) {
        this.childPosition = childPosition;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getActiveUserForgot() {
        return activeUserForgot;
    }

    public void setActiveUserForgot(String activeUserForgot) {
        this.activeUserForgot = activeUserForgot;
    }

    public UserLogin getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(UserLogin userLogin) {
        this.userLogin = userLogin;
    }

    public Project getChooser() {
        return chooser;
    }

    public void setChooser(Project chooser) {
        this.chooser = chooser;
    }

    public Rate getChildren() {
        return children;
    }

    public void setChildren(Rate children) {
        this.children = children;
    }
}
