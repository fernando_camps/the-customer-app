package com.example.fcc.thecustomerapp.rest.requests;

import com.example.fcc.thecustomerapp.rest.models.GetProjectLoginRequest;
import com.example.fcc.thecustomerapp.rest.models.GetProjectLoginResponse;
import com.example.fcc.thecustomerapp.rest.models.GetProjectRateRequest;
import com.example.fcc.thecustomerapp.rest.models.GetProjectRateResponse;
import com.example.fcc.thecustomerapp.rest.webservices.LoginWebService;
import com.example.fcc.thecustomerapp.rest.webservices.RateWebService;

/**
 * Created by fcc on 16/02/16.
 */
public class RateRequest extends BaseRequest<GetProjectRateResponse, RateWebService>{

    private GetProjectRateRequest rateRequest;

    public RateRequest(String request, int idProject, String token, String tokenRefresh){
        super(GetProjectRateResponse.class, RateWebService.class);

        rateRequest = new GetProjectRateRequest();

        rateRequest.setRequest(request);
        rateRequest.setIdProject(idProject);
        rateRequest.setToken(token);
        rateRequest.setTokenRefresh(tokenRefresh);
    }

    @Override
    public GetProjectRateResponse loadDataFromNetwork() throws Exception {
        return getService().getRate(rateRequest);
    }
}
