package com.example.fcc.thecustomerapp.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.fcc.thecustomerapp.R;
import com.example.fcc.thecustomerapp.components.Constants;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by fcc on 2/02/16.
 */

public class Util {
    private static Toast toastEmail;

    public static void sendEmail(Context context, String email) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("message/rfc822");

        PackageManager packageManager = context.getPackageManager();
        List activities = packageManager.queryIntentActivities(emailIntent,PackageManager.MATCH_DEFAULT_ONLY);
        boolean isIntentSafe = activities.size() > 0;

        if (isIntentSafe) {
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Email subject");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message text");
            emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(emailIntent);
        } else {
            if (toastEmail!=null){
                toastEmail.cancel();
            }
            toastEmail = Toast.makeText(context,context.getString(R.string.error_no_email_client),Toast.LENGTH_SHORT);
            toastEmail.show();
        }

    }

    public static void message(String message) {
        Log.d("My tag", message);
    }

    public static boolean testConexion(Context ctx) {

        if (ctx!=null){
            ConnectivityManager connectivityManager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Network[] networks = connectivityManager.getAllNetworks();
                NetworkInfo networkInfo;
                for (Network mNetwork : networks) {
                    networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                    if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                        return true;
                    }
                }
            }else {
                if (connectivityManager != null) {
                    NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                    if (info != null) {
                        for (NetworkInfo anInfo : info) {
                            if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                                Log.d("Network",
                                        "NETWORKNAME: " + anInfo.getTypeName());
                                return true;
                            }
                        }
                    }
                }
            }
            Toast.makeText(ctx,ctx.getString(R.string.please_connect_to_internet),Toast.LENGTH_SHORT).show();
            return false;
        }
            return false;
    }

    public static void errorMessage(Context context, int number) {

        switch (number) {
            case 1:
                Toast.makeText(context,context.getString(R.string.error_io),Toast.LENGTH_SHORT).show();
                break;

            case 2:
                Toast.makeText(context,context.getString(R.string.error_json),Toast.LENGTH_SHORT).show();
                break;
            case 300:
                Util.message(Constants.ERROR_JSON_CODE_300);
                break;

            case 301:
                Util.message(Constants.ERROR_JSON_CODE_301);
                break;

            case 302:
                Util.message(Constants.ERROR_JSON_CODE_302);
                break;

            case 303:
                Util.message(Constants.ERROR_JSON_CODE_303);
                break;
        }
    }

    public static void downloadFile(String imageHttpAddress, ImageView imageView, Bitmap loadedImage, Context context) {
        URL imageUrl = null;
        try {
            imageUrl = new URL(imageHttpAddress);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.connect();
            loadedImage = BitmapFactory.decodeStream(conn.getInputStream());
            imageView.setImageBitmap(loadedImage);
        } catch (IOException e) {
            Toast.makeText(context, Constants.DOWNLOAD_ERROR + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus().getWindowToken()!=null){
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }

    }
}
