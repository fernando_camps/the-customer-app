package com.example.fcc.thecustomerapp.activitys;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.example.fcc.thecustomerapp.R;
import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.components.InterfaceMain;
import com.example.fcc.thecustomerapp.event.EventFragments;
import com.example.fcc.thecustomerapp.fragments.FragmentClose_;
import com.example.fcc.thecustomerapp.fragments.FragmentCorrectRestore_;
import com.example.fcc.thecustomerapp.fragments.FragmentDetails_;
import com.example.fcc.thecustomerapp.fragments.FragmentForgotPass_;
import com.example.fcc.thecustomerapp.fragments.FragmentIncorrrectRestore_;
import com.example.fcc.thecustomerapp.fragments.FragmentListProyects_;
import com.example.fcc.thecustomerapp.fragments.FragmentLogin_;
import com.example.fcc.thecustomerapp.fragments.FragmentRateMsg_;
import com.example.fcc.thecustomerapp.fragments.FragmentRate_;
import com.example.fcc.thecustomerapp.fragments.FragmentRated_;
import com.example.fcc.thecustomerapp.fragments.FragmentSplash_;
import com.example.fcc.thecustomerapp.models.UserLogin;
import com.example.fcc.thecustomerapp.utilities.Util;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity implements InterfaceMain {

    //region Variables

    //region Constants X
    //endregion

    //region Primitives X
    //endregion

    //region Objects
    private UserLogin userLogin;
    //endregion

    //region Views
    @ViewById
    ProgressBar progressBar;
    @ViewById
    Toolbar toolbar;
    //endregion

    //endregion

    //region Component life cycle X
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);

    }

    @AfterViews
    public void main(){
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.icon_garra_1);
        toolbar.setTitle(Constants.TITLE_TOOLBAR_LIST);
        toolbar.inflateMenu(R.menu.menu_main);
        toolbar.setSubtitleTextColor(Color.WHITE);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setOverflowIcon(getDrawable(R.drawable.btn_menu_puntos_1));
        } else {
            toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.btn_menu_puntos_1));
        }

        initListeners();

        progressBar.setVisibility(View.INVISIBLE);

        FragmentSplash_ fragmentSplash = (FragmentSplash_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_SPLASH);

        if (fragmentSplash != null) {
            getSupportFragmentManager().beginTransaction().remove(fragmentSplash).commit();
        }

        fragmentSplash = new FragmentSplash_();

        getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, fragmentSplash, Constants.TAG_SPLASH).
                show(fragmentSplash).commit();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
    //onAttach, onResume, onCreate, onPause, onStop, onDestroy, activityForResult

    //endregion

    //region Methods X

    //region UI clicks X
    //endregion

    //region Actionbar clicks X
    //endregion

    //region Private

    private void showLogOut(){
        FragmentClose_ fragmentClose = (FragmentClose_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_CLOSE);
        if (fragmentClose == null) {
            fragmentClose = new FragmentClose_();
            fragmentClose.setUserLogin(userLogin);

            getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, fragmentClose, Constants.TAG_CLOSE).
                    show(fragmentClose).
                    commit();
        }
    }
    //endregion

    //region Protected X
    //endregion

    //region Public
    public void initListeners(){
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.action_email_list) {
                    if (!Util.testConexion(getApplicationContext())) {
                    } else {
                        Util.sendEmail(getApplicationContext(), "tecnicamps@gmail.com");
                    }

                    return true;
                } else if (id == R.id.action_exit_list) {
                    showLogOut();

                }

                return onOptionsItemSelected(item);
            }
        });
    }

    //region Getters X
    //endregion

    //region Setters X
    //endregion

    //endregion

    //region Super override
    @Override
    public void onBackPressed() {

        FragmentForgotPass_ fragmentForgot = (FragmentForgotPass_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_FORGOT);
        FragmentListProyects_ fragmentLista = (FragmentListProyects_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_LIST);
        FragmentLogin_ fragmentLogin = (FragmentLogin_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_LOGIN);
        FragmentDetails_ fragmentDetails = (FragmentDetails_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_DETAILS);
        FragmentRate_ fragmentRate = (FragmentRate_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_RATE);
        FragmentRated_ fragmentRated = (FragmentRated_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_RATED);
        FragmentRateMsg_ fragmentRatedMsg = (FragmentRateMsg_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_RATED_MSG);
        FragmentClose_ fragmentClose = (FragmentClose_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_CLOSE);
        FragmentCorrectRestore_ fragmentCorrectRestore = (FragmentCorrectRestore_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_CORRECT_RESTORE);
        FragmentIncorrrectRestore_ fragmentIncorrectRestore = (FragmentIncorrrectRestore_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_INCORRECT_RESTORE);

        if (progressBar!=null){
            hideProgress();
        }

        if (fragmentIncorrectRestore != null && fragmentIncorrectRestore.isVisible()) {
            getSupportFragmentManager().beginTransaction().remove(fragmentIncorrectRestore).commit();
        } else if (fragmentCorrectRestore != null && fragmentCorrectRestore.isVisible()) {
            getSupportFragmentManager().beginTransaction().remove(fragmentCorrectRestore).commit();
        } else if (fragmentForgot != null && fragmentForgot.isVisible()) {
            getSupportFragmentManager().beginTransaction().remove(fragmentForgot).commit();
            fragmentLogin.activateLogin();

        } else if (fragmentRatedMsg != null && fragmentRatedMsg.isVisible()) {
            getSupportFragmentManager().beginTransaction().remove(fragmentRatedMsg).commit();
        } else if (fragmentClose != null && fragmentClose.isVisible()) {
            getSupportFragmentManager().beginTransaction().remove(fragmentClose).commit();

        } else if (fragmentRated != null && fragmentRated.isVisible()) {
            getSupportFragmentManager().beginTransaction().remove(fragmentRated).commit();
            toolbar.setTitle(Constants.TITLE_TOOLBAR_LIST);
        } else if (fragmentRate != null && fragmentRate.isVisible()) {
            getSupportFragmentManager().beginTransaction().remove(fragmentRate).commit();
            toolbar.setTitle(Constants.TITLE_TOOLBAR_LIST);
        } else if (fragmentDetails != null && fragmentDetails.isVisible()) {
            getSupportFragmentManager().beginTransaction().remove(fragmentDetails).commit();
            toolbar.setTitle(Constants.TITLE_TOOLBAR_LIST);
        } else if (fragmentLista != null && fragmentLista.isVisible()) {
            super.onBackPressed();
        } else if (fragmentLogin != null && fragmentLogin.isVisible()) {
            super.onBackPressed();
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }
    //endregion

    //region Interface implementations X

    //region example-onClickListener
    //endregion

    //region example-onLocationListener
    //endregion

    //region example-onCustomActionListener
    //endregion

    //endregion

    //endregion

    //region Inner definitions X

    //region Interfaces X

    //endregion

    //region Classes X
    //endregion

    //endregion

    //region Events
    @Subscribe
    public void onEvent(EventFragments event){
        if (userLogin==null){
            this.userLogin = event.getUserLogin();
        }

        FragmentSplash_ fragmentSplash = (FragmentSplash_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_SPLASH);
        FragmentLogin_ fragmentLogin = (FragmentLogin_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_LOGIN);
        FragmentForgotPass_ fragmentForgotPass = (FragmentForgotPass_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_FORGOT);
        FragmentCorrectRestore_ fragmentCorrectRestore = (FragmentCorrectRestore_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_CORRECT_RESTORE);
        FragmentIncorrrectRestore_ fragmentIncorrectRestore = (FragmentIncorrrectRestore_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_INCORRECT_RESTORE);
        FragmentListProyects_ fragmentListProyects = (FragmentListProyects_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_LIST);
        FragmentDetails_ fragmentDetails = (FragmentDetails_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_DETAILS);
        FragmentRate_ fragmentRate = (FragmentRate_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_RATE);
        FragmentRated_ fragmentRated = (FragmentRated_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_RATED);
        FragmentRateMsg_ fragmentRateMsg = (FragmentRateMsg_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_RATED_MSG);
        FragmentClose_ fragmentClose = (FragmentClose_) getSupportFragmentManager().findFragmentByTag(Constants.TAG_CLOSE);

        switch (event.getTag()){
            case Constants.TAG_LOGIN:
                toolbar.setVisibility(View.GONE);

                if (fragmentSplash != null) {
                    getSupportFragmentManager().beginTransaction().remove(fragmentSplash).commit();
                }
                if (fragmentLogin == null) {
                    fragmentLogin = new FragmentLogin_();
                    getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, fragmentLogin, Constants.TAG_LOGIN).
                            show(fragmentLogin).
                            commit();

                    fragmentLogin.setInterfaceMain(this);
                }

                break;

            case Constants.TAG_FORGOT:
                if (fragmentForgotPass == null) {
                    fragmentForgotPass = new FragmentForgotPass_();

                    fragmentForgotPass.setInterfaceMain(this);

                    getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, fragmentForgotPass, Constants.TAG_FORGOT).
                            show(fragmentForgotPass).
                            commit();
                }

                break;

            case Constants.TAG_CORRECT_RESTORE:
                if (fragmentForgotPass!=null){
                    getSupportFragmentManager().beginTransaction().remove(fragmentForgotPass).commit();
                }
                if (fragmentCorrectRestore == null) {
                    fragmentCorrectRestore = new FragmentCorrectRestore_();
                    fragmentCorrectRestore.setActiveUserForgot(event.getActiveUserForgot());

                    getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, fragmentCorrectRestore, Constants.TAG_CORRECT_RESTORE).
                            show(fragmentCorrectRestore).
                            commit();
                }
                break;

            case Constants.TAG_INCORRECT_RESTORE:
                if (fragmentIncorrectRestore == null) {
                    fragmentIncorrectRestore = new FragmentIncorrrectRestore_();

                    fragmentIncorrectRestore.setActiveUser(event.getActiveUserForgot());

                    getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, fragmentIncorrectRestore, Constants.TAG_INCORRECT_RESTORE).
                            show(fragmentIncorrectRestore).
                            commit();
                }
                break;

            case  Constants.TAG_FORGOT_ACCEPT:
                if (fragmentForgotPass != null) {
                    getSupportFragmentManager().beginTransaction().remove(fragmentForgotPass).commit();
                }
                if (fragmentCorrectRestore != null) {
                    getSupportFragmentManager().beginTransaction().remove(fragmentCorrectRestore).commit();
                }
                if (fragmentLogin != null) {
                    fragmentLogin.activateLogin();
                }
                break;

            case Constants.TAG_FORGOT_CANCEL:
                if (fragmentLogin != null) {
                    fragmentLogin.activateLogin();
                }
                if (fragmentForgotPass != null) {
                    getSupportFragmentManager().beginTransaction().remove(fragmentForgotPass).commit();
                }
                break;

            case Constants.TAG_FORGOT_REINTENT:
                if (fragmentForgotPass == null) {
                    fragmentForgotPass = new FragmentForgotPass_();

                    getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, fragmentForgotPass, Constants.TAG_FORGOT).
                            show(fragmentForgotPass).
                            commit();
                }
                break;

            case Constants.TAG_LIST:
                toolbar.setVisibility(View.VISIBLE);

                if (fragmentLogin != null) {
                    getSupportFragmentManager().beginTransaction().remove(fragmentLogin).commit();
                }

                if (fragmentListProyects == null) {

                    fragmentListProyects = new FragmentListProyects_();
                    fragmentListProyects.setInterfaceMain(this);
                    fragmentListProyects.setUserLogin(event.getUserLogin());

                    toolbar.setTitle(Constants.TITLE_TOOLBAR_LIST);

                    getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, fragmentListProyects, Constants.TAG_LIST).
                            show(fragmentListProyects).
                            commit();
                }
                break;

            case Constants.TAG_DETAILS:
                if (fragmentDetails == null) {
                    fragmentDetails = new FragmentDetails_();

                    fragmentDetails.setChooser(event.getChooser());
                    fragmentDetails.setUserLogin(event.getUserLogin());
                    fragmentDetails.setInterfaceMain(this);

                    getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, fragmentDetails, Constants.TAG_DETAILS).
                            show(fragmentDetails).
                            commit();
                }
                break;

            case Constants.TAG_RATE:
                if (fragmentRated != null) {
                    getSupportFragmentManager().beginTransaction().remove(fragmentRated).commit();
                }
                if (fragmentRate == null) {
                    fragmentRate = new FragmentRate_();

                    toolbar.setTitle(Constants.TITLE_TOOLBAR_RATE);

                    fragmentRate.setChooser(event.getChooser());
                    fragmentRate.setUserLogin(event.getUserLogin());
                    fragmentRate.setInterfaceMain(this);

                    getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, fragmentRate, Constants.TAG_RATE).
                            show(fragmentRate).
                            commit();
                }
                break;

            case Constants.TAG_RATED_MSG:
                if (fragmentRateMsg == null) {
                    fragmentRateMsg = new FragmentRateMsg_();

                    fragmentRateMsg.setCorrectRate(event.getChildren().getRate(), event.getChildren().getId(), event.getUserLogin().getToken(),event.getUserLogin().getTokenRefresh());
                    fragmentRateMsg.setInterfaceMain(this);

                    getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, fragmentRateMsg, Constants.TAG_RATED_MSG).
                            show(fragmentRateMsg).
                            commit();
                }
                break;

            case Constants.TAG_RATED:
                if (fragmentRate != null) {
                    getSupportFragmentManager().beginTransaction().remove(fragmentRate).commit();
                }
                if (fragmentRated == null) {
                    fragmentRated = new FragmentRated_();

                    fragmentRated.setProyectList(event.getChooser());
                    fragmentRated.setUserLogin(event.getUserLogin());

                    fragmentRated.setInterfaceMain(this);

                    getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, fragmentRated, Constants.TAG_RATED).
                            show(fragmentRated).
                            commit();
                }
                break;

            case Constants.TAG_CLOSE_ACCEPT:
                if (fragmentRate != null) {
                    getSupportFragmentManager().beginTransaction().remove(fragmentRate).commit();
                }
                if (fragmentRated != null) {
                    getSupportFragmentManager().beginTransaction().remove(fragmentRated).commit();
                }
                if (fragmentDetails != null) {
                    getSupportFragmentManager().beginTransaction().remove(fragmentDetails).commit();
                }

                if (fragmentListProyects != null) {
                    getSupportFragmentManager().beginTransaction().remove(fragmentListProyects).commit();
                }

                if (fragmentLogin == null) {
                    fragmentLogin = new FragmentLogin_();

                    fragmentLogin.setInterfaceMain(this);
                    toolbar.setVisibility(View.GONE);

                    getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, fragmentLogin, Constants.TAG_LOGIN).
                            show(fragmentLogin).
                            commit();
                }
                break;

            case Constants.TAG_CLOSE_CANCEL:
                if (fragmentClose != null) {
                    getSupportFragmentManager().beginTransaction().remove(fragmentClose).commit();
                }
                break;
        }

    }
    //endregion

}



