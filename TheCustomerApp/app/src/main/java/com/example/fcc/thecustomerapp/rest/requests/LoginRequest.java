package com.example.fcc.thecustomerapp.rest.requests;

import com.example.fcc.thecustomerapp.rest.models.GetProjectLoginRequest;
import com.example.fcc.thecustomerapp.rest.models.GetProjectLoginResponse;
import com.example.fcc.thecustomerapp.rest.webservices.LoginWebService;

/**
 * Created by fcc on 15/02/16.
 */
public class LoginRequest extends BaseRequest<GetProjectLoginResponse, LoginWebService>{

    private GetProjectLoginRequest loginRequest;

    public LoginRequest(String userLogin, String userPass){
        super(GetProjectLoginResponse.class, LoginWebService.class);

        loginRequest = new GetProjectLoginRequest();

        loginRequest.setUserLogin(userLogin);
        loginRequest.setUserPass(userPass);
    }

    @Override
    public GetProjectLoginResponse loadDataFromNetwork() throws Exception {
        return getService().getLogin(loginRequest);
    }
}
