package com.example.fcc.thecustomerapp.rest.common;

import android.app.Application;

import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.rest.models.GetProjectDetailsResponse;
import com.example.fcc.thecustomerapp.rest.models.GetProjectForgotResponse;
import com.example.fcc.thecustomerapp.rest.models.GetProjectListResponse;
import com.example.fcc.thecustomerapp.rest.models.GetProjectLoginResponse;
import com.example.fcc.thecustomerapp.rest.models.GetProjectRateResponse;
import com.example.fcc.thecustomerapp.rest.models.GetProjectRatedResponse;
import com.example.fcc.thecustomerapp.rest.models.GetRateMsgResponse;
import com.example.fcc.thecustomerapp.rest.webservices.ProjectListWebService;
import com.example.fcc.thecustomerapp.utilities.Util;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

import retrofit.RestAdapter;

/**
 * Created by irving on 5/03/15.
 */

public class RestService extends RetrofitGsonSpiceService {

    private static final String TAG = "RestService";
    private static final String BASE_URL = Constants.URL_BASE;

    @Override
    public void onCreate(){
        super.onCreate();
        addRetrofitInterface(ProjectListWebService.class);
    }


    @Override
    protected RestAdapter.Builder createRestAdapterBuilder() {
        RestAdapter.Builder builder = super.createRestAdapterBuilder();

        builder.setLogLevel(RestAdapter.LogLevel.FULL);
        builder.setLog(new RestAdapter.Log() {

            @Override
            public void log(String message) {
                Util.message(message);
            }
        });

        return builder;
    }

    @Override
    protected String getServerUrl() {
        return BASE_URL;
    }

    @Override
    public CacheManager createCacheManager(Application application) throws CacheCreationException {
        CacheManager manager = new CacheManager();
        GsonObjectPersister<GetProjectListResponse> persisterProjectList = new GsonObjectPersister<GetProjectListResponse>(application, GetProjectListResponse.class);
        GsonObjectPersister<GetProjectLoginResponse> persisterLogin = new GsonObjectPersister<GetProjectLoginResponse>(application, GetProjectLoginResponse.class);
        GsonObjectPersister<GetProjectDetailsResponse> persisterDetails = new GsonObjectPersister<GetProjectDetailsResponse>(application, GetProjectDetailsResponse.class);
        GsonObjectPersister<GetProjectForgotResponse> persisterForgot = new GsonObjectPersister<GetProjectForgotResponse>(application, GetProjectForgotResponse.class);
        GsonObjectPersister<GetProjectRateResponse> persisterRate = new GsonObjectPersister<GetProjectRateResponse>(application, GetProjectRateResponse.class);
        GsonObjectPersister<GetProjectRatedResponse> persisterRated = new GsonObjectPersister<GetProjectRatedResponse>(application, GetProjectRatedResponse.class);
        GsonObjectPersister<GetRateMsgResponse> persisterRateMsg = new GsonObjectPersister<GetRateMsgResponse>(application, GetRateMsgResponse.class);


        manager.addPersister(persisterProjectList);
        manager.addPersister(persisterLogin);
        manager.addPersister(persisterDetails);
        manager.addPersister(persisterForgot);
        manager.addPersister(persisterRate);
        manager.addPersister(persisterRated);
        manager.addPersister(persisterRateMsg);

        return manager;
    }

//    @Override
//    public int getThreadCount() {
//        return 10;
//    }
}
