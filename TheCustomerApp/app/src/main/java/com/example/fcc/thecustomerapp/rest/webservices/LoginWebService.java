package com.example.fcc.thecustomerapp.rest.webservices;

import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.rest.models.GetProjectListRequest;
import com.example.fcc.thecustomerapp.rest.models.GetProjectListResponse;
import com.example.fcc.thecustomerapp.rest.models.GetProjectLoginRequest;
import com.example.fcc.thecustomerapp.rest.models.GetProjectLoginResponse;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by fcc on 15/02/16.
 */
public interface LoginWebService {
    @POST(Constants.URL_LOGIN)
    GetProjectLoginResponse getLogin(@Body GetProjectLoginRequest body);
}
