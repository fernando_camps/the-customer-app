package com.example.fcc.thecustomerapp.rest.models;

import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.models.UserLogin;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fcc on 15/02/16.
 */
public class GetProjectLoginResponse {

    @SerializedName(Constants.JSON_CODE)
    private int code;
    @SerializedName(Constants.JSON_MSG)
    private String message;
    @SerializedName(Constants.JSON_USER)
    private UserLogin userLogin;

    public UserLogin getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(UserLogin userLogin) {
        this.userLogin = userLogin;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
