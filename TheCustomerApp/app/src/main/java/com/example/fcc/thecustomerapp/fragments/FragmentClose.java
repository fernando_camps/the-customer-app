package com.example.fcc.thecustomerapp.fragments;

import android.support.v4.app.Fragment;
import android.widget.TextView;

import com.example.fcc.thecustomerapp.R;
import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.event.EventFragments;
import com.example.fcc.thecustomerapp.models.UserLogin;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

@EFragment(R.layout.activity_fragment_close)
public class FragmentClose extends Fragment {
    private UserLogin userLogin;

    @ViewById
    TextView tvActiveUser;

    @AfterViews
    public void main(){
        tvActiveUser.setText(userLogin.getEmail().toString());
    }

    @Click(R.id.btnCancel)
    protected void onCancel(){

        EventFragments event = new EventFragments();
        event.setTag(Constants.TAG_CLOSE_CANCEL);
        EventBus.getDefault().post(event);

    }

    @Click(R.id.btnAccept)
    protected void onAccept(){
        getActivity().onBackPressed();

        EventFragments event = new EventFragments();
        event.setTag(Constants.TAG_CLOSE_ACCEPT);
        EventBus.getDefault().post(event);

    }

    public void setUserLogin(UserLogin userLogin) {
        this.userLogin = userLogin;
    }

}