package com.example.fcc.thecustomerapp.rest.webservices;

import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.rest.models.GetRateMsgRequest;
import com.example.fcc.thecustomerapp.rest.models.GetRateMsgResponse;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by fcc on 16/02/16.
 */
public interface RateMsgWebService {
    @POST(Constants.URL_RATE_MSG)
    GetRateMsgResponse getRateMsg(@Body GetRateMsgRequest body);
}
