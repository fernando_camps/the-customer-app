package com.example.fcc.thecustomerapp.components;

import com.example.fcc.thecustomerapp.models.Rate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fcc on 20/01/16.
 */
public class GroupItems {

    public String string;
    public Rate rate;
    public final List<Rate> children = new ArrayList<Rate>();
    public final List<String> childrens = new ArrayList<String>();

    public GroupItems(Rate rate) {
        this.rate = rate;
    }

    public GroupItems(String string) {
        this.string = string;
    }

}