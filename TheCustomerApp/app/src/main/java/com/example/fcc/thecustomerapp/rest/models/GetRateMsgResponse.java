package com.example.fcc.thecustomerapp.rest.models;

import com.example.fcc.thecustomerapp.components.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fcc on 16/02/16.
 */
public class GetRateMsgResponse  {
    @SerializedName(Constants.JSON_CODE)
    private int code;
    @SerializedName(Constants.JSON_MSG)
    private String message;
    @SerializedName(Constants.RATE_RESULT_MSG)
    private boolean result;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isResult() {
        return result;
    }
    public void setResult(boolean result) {
        this.result = result;
    }
}
