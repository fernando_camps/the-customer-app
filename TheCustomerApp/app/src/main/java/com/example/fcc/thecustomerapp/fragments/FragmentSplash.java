package com.example.fcc.thecustomerapp.fragments;

import android.support.v4.app.Fragment;

import com.example.fcc.thecustomerapp.R;
import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.event.EventFragments;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.greenrobot.eventbus.EventBus;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by fcc on 5/02/16.
 */

@EFragment(R.layout.activity_splash_welcome)
public class FragmentSplash extends Fragment {
    private Timer timerSplash;

    @AfterViews
    public void main(){
        initTimer();
    }

    private void initTimer() {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                EventFragments event = new EventFragments();
                event.setTag(Constants.TAG_LOGIN);
                EventBus.getDefault().post(event);

            }
        };

        timerSplash = new Timer();
        timerSplash.schedule(task, Constants.SPLASH_SCREEN_DELAY);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (timerSplash != null) {
            timerSplash.cancel();
        }

    }
}
