package com.example.fcc.thecustomerapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.example.fcc.thecustomerapp.R;
import com.example.fcc.thecustomerapp.adapters.AdapterExpandable;
import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.components.GroupItems;
import com.example.fcc.thecustomerapp.components.InterfaceMain;
import com.example.fcc.thecustomerapp.event.EventFragments;
import com.example.fcc.thecustomerapp.models.Project;
import com.example.fcc.thecustomerapp.models.Rate;
import com.example.fcc.thecustomerapp.models.Tecnology;
import com.example.fcc.thecustomerapp.models.UserLogin;
import com.example.fcc.thecustomerapp.rest.common.SpiceHelper;
import com.example.fcc.thecustomerapp.rest.models.GetProjectDetailsResponse;
import com.example.fcc.thecustomerapp.rest.models.GetProjectRateResponse;
import com.example.fcc.thecustomerapp.rest.requests.DetailsRequest;
import com.example.fcc.thecustomerapp.rest.requests.RateRequest;
import com.example.fcc.thecustomerapp.utilities.Util;
import com.octo.android.robospice.persistence.exception.SpiceException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;

@EFragment(R.layout.activity_fragment_rate)
public class FragmentRate extends BaseRestFragment{

    //region Variables X

    //region Constants X
    //endregion

    //region Primitives
    private boolean android;
    private boolean ios;
    private boolean web;
    //endregion

    //region Objects
    private InterfaceMain listenerMain;
    private Project proyectList;
    private Project project;
    private Rate rate;
    private UserLogin userLogin;
    private ArrayList<Rate> rateArrayList;
    private ArrayList<GroupItems> grupo;
    private SpiceHelper<GetProjectRateResponse>spiceProjectRate;
    private SpiceHelper<GetProjectDetailsResponse>spiceProjectDetails;
    //endregion

    //region Views
    @ViewById
    ExpandableListView listViewRate;
    @ViewById
    TextView tabRated;
    @ViewById
    TextView tvTitleRate;
    //endregion

    //endregion

    //region Component life cycle

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.activity_fragment_rate, container, false);
    }

    @AfterViews
    public void main(){
        initListeners();

        rate = new Rate();
        rate.setStatus(Constants.RATE_STATUS_NULL);

        tvTitleRate.setText(proyectList.getTitleProyect());

        spiceProjectDetails = new SpiceHelper<GetProjectDetailsResponse>(getSpiceManager(), "CACHE_LIST", GetProjectDetailsResponse.class) {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                Util.message(Constants.ERROR_ROBOSPICE_PETICION);
                exception.printStackTrace();

                if (listenerMain != null) {
                    listenerMain.hideProgress();
                }

                Util.errorMessage(getContext(), 2);
            }

            @Override
            protected void onSuccess(GetProjectDetailsResponse response) {
                super.onSuccess(response);

                int code = response.getCode();

                if (code == 200) {

                    project = new Project();
                    project = response.getProject();

                    for (int i = 0; i < project.getProjectList().size(); i++) {

                        Tecnology projectListElement = new Tecnology();
                        projectListElement = project.getProjectList().get(i);

                        //TECNOLOGY ANDROID
                        if (projectListElement.getTecnology().equals(Constants.TEC_ANDROID)) {
                            android = true;
                        }

                        //TECNOLOGY IOS
                        if (projectListElement.getTecnology().equals(Constants.TEC_IOS)) {
                            ios = true;
                        }

                        //TECNOLOGY WEB
                        if (projectListElement.getTecnology().equals(Constants.TEC_WEB)) {
                            web = true;
                        }

                    }
                }
            }};

        DetailsRequest request1 = new DetailsRequest(Constants.JSON_SOLICITUDE_DETAIL,proyectList.getIdProyect(),userLogin.getToken(),userLogin.getTokenRefresh());

        spiceProjectDetails.executeRequest(request1);



        spiceProjectRate = new SpiceHelper<GetProjectRateResponse>(getSpiceManager(), "CACHE_LIST", GetProjectRateResponse.class) {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                Util.message("Fallo la petición web");
                exception.printStackTrace();

                if (listenerMain != null) {
                    listenerMain.hideProgress();
                }

                Util.errorMessage(getContext(), 2);
            }

            @Override
            protected void onSuccess(GetProjectRateResponse response) {
                super.onSuccess(response);

                int code = response.getCode();

                if (code == 200) {

                    grupo = new ArrayList<>();

                    if (response.getRates().size() == 0) {
                        ;
                        if (android){
                            GroupItems group1 = new GroupItems(Constants.RATE_TEC_ANDROID);
                            group1.children.add(rate);
                            grupo.add(group1);
                        }
                        if (ios){
                            GroupItems group2 = new GroupItems(Constants.RATE_TEC_IOS);
                            group2.children.add(rate);
                            grupo.add(group2);
                        }
                        if (web){
                            GroupItems group3 = new GroupItems(Constants.RATE_TEC_WEB);
                            group3.children.add(rate);
                            grupo.add(group3);
                        }

                    } else {

                        rateArrayList = new ArrayList<>();
                        ArrayList<Rate> orderAndroid = new ArrayList<>();
                        ArrayList<Rate> orderIos = new ArrayList<>();
                        ArrayList<Rate> orderWeb = new ArrayList<>();

                        for (int i = 0; i < response.getRates().size(); i++) {

                            Rate rateListElement = new Rate();
                            rateListElement = response.getRates().get(i);

                            String numberSprint = rateListElement.getSprint().substring(0, 1);

                            rateListElement.setSprint(numberSprint);

                            if (rateListElement.getTecnology().equals(Constants.TEC_ANDROID)) {
                                orderAndroid.add(rateListElement);

                            }
                            if (rateListElement.getTecnology().equals(Constants.TEC_IOS)) {
                                orderIos.add(rateListElement);
                            }
                            if (rateListElement.getTecnology().equals(Constants.TEC_WEB)) {
                                orderWeb.add(rateListElement);
                            }

                            rateArrayList.add(rateListElement);

                            if (rateArrayList.size() == response.getRates().size()) {
                                if (orderAndroid.size() != 0) {
                                    GroupItems group = new GroupItems(Constants.RATE_TEC_ANDROID);
                                    Collections.sort(orderAndroid);
                                    group.children.addAll(orderAndroid);
                                    grupo.add(group);
                                }else {
                                    if (android){
                                        GroupItems group1 = new GroupItems(Constants.RATE_TEC_ANDROID);
                                        group1.children.add(rate);
                                        grupo.add(group1);
                                    }
                                }
                                if (orderIos.size() != 0) {
                                    GroupItems group = new GroupItems(Constants.RATE_TEC_IOS);
                                    Collections.sort(orderIos);
                                    group.children.addAll(orderIos);
                                    grupo.add(group);
                                }else {
                                    if (ios){
                                        GroupItems group2 = new GroupItems(Constants.RATE_TEC_IOS);
                                        group2.children.add(rate);
                                        grupo.add(group2);
                                    }
                                }
                                if (orderWeb.size() != 0) {
                                    GroupItems group = new GroupItems(Constants.RATE_TEC_WEB);
                                    Collections.sort(orderWeb);
                                    group.children.addAll(orderWeb);
                                    grupo.add(group);
                                }else {
                                    if (web) {
                                        GroupItems group3 = new GroupItems(Constants.RATE_TEC_WEB);
                                        group3.children.add(rate);
                                        grupo.add(group3);
                                    }
                                }
                                rowAdapter();
                            }

                        }

                    }
                    rowAdapter();

                    if (listenerMain != null) {
                        listenerMain.hideProgress();
                    }

                } else {

                    if (listenerMain != null) {
                        listenerMain.hideProgress();
                    }

                    if (code == 300) {
                        Util.message(response.getMessage());
                        Util.errorMessage(getContext(), 2);
                    }
                    if (code == 301) {
                        Util.message(response.getMessage());
                        Util.errorMessage(getContext(), 2);
                    }

                }
            }

        };

        RateRequest request2 = new RateRequest(Constants.JSON_SOLICITUDE_RATE,proyectList.getIdProyect(),userLogin.getToken().toString(),userLogin.getTokenRefresh().toString());

        spiceProjectRate.executeRequest(request2);

        if (listenerMain!=null){
            listenerMain.showProgress();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);

        if (spiceProjectRate!=null){
            spiceProjectRate.clearCacheRequest();
        }

    }

    //onAttach, onResume, onCreate, onPause, onStop, onDestroy, activityForResult

    //endregion

    //region Methods X

    //region UI clicks
    @Click(R.id.tabRated)
    protected void clickRated(){

        if (!Util.testConexion(getContext())) {
        } else {

            EventFragments event = new EventFragments();
            event.setTag(Constants.TAG_RATED);
            event.setUserLogin(userLogin);
            event.setChooser(proyectList);

            EventBus.getDefault().post(event);

        }
    }
    //endregion

    //region Actionbar clicks X
    //endregion

    //region Private
    private void rowAdapter() {
        AdapterExpandable adapter = new AdapterExpandable(getContext(), grupo, userLogin);
        listViewRate.setAdapter(adapter);
    }
    //endregion

    //region Protected X
    //endregion

    //region Public

    public void initListeners() {
        listViewRate.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               /* Rate elegido = (Rate) parent.getItemAtPosition(position);

                CharSequence texto = "Seleccionado: " + elegido.getRate();
                Toast toast = Toast.makeText(getActivity(), texto, Toast.LENGTH_LONG);
                toast.show();*/
            }
        });
    }

    public void setChooser(Project chooser) {
        this.proyectList = chooser;
    }

    public void setUserLogin(UserLogin userLogin) {
        this.userLogin = userLogin;
    }

    public void setInterfaceMain(InterfaceMain listenerMain) {
        this.listenerMain = listenerMain;
    }

    //region Getters X
    //endregion

    //region Setters X
    //endregion

    //endregion

    //region Super override
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem menuItem = menu.findItem(R.id.action_email_list);
        menuItem.setVisible(false);
    }
    //endregion

    //region Interface implementations X

    //region example-onClickListener
    //endregion

    //region example-onLocationListener
    //endregion

    //region example-onCustomActionListener
    //endregion

    //endregion

    //endregion

    //region Inner definitions X

    //region Interfaces X

    //endregion

    //region Classes X
    //endregion

    //endregion
}