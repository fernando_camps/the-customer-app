package com.example.fcc.thecustomerapp.rest.models;

import com.example.fcc.thecustomerapp.components.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fcc on 16/02/16.
 */
public class GetRateMsgRequest {

    @SerializedName(Constants.JSON_SOLICITUDE)
    private String request;
    @SerializedName(Constants.JSON_RATED_MSG_ID)
    private String idProject;
    @SerializedName(Constants.JSON_RATED_MSG_RATE)
    private int rate;
    @SerializedName(Constants.JSON_RATED_MSG_TOKEN)
    private String token;
    @SerializedName(Constants.JSON_RATED_MSG_REFRESHTOKEN)
    private String tokenRefresh;

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenRefresh() {
        return tokenRefresh;
    }

    public void setTokenRefresh(String tokenRefresh) {
        this.tokenRefresh = tokenRefresh;
    }



}
