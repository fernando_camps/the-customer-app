package com.example.fcc.thecustomerapp.rest.models;

import com.example.fcc.thecustomerapp.components.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fcc on 16/02/16.
 */
public class GetProjectForgotRequest {


    @SerializedName(Constants.JSON_CONTACT_EMAIL)
    private String contactEmail;
    @SerializedName(Constants.JSON_SOLICITUDE)
    private String request;

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }
}
