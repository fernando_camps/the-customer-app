package com.example.fcc.thecustomerapp.rest.webservices;

import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.models.Project;
import com.example.fcc.thecustomerapp.rest.models.GetProjectListRequest;
import com.example.fcc.thecustomerapp.rest.models.GetProjectListResponse;

import retrofit.http.Body;
import retrofit.http.POST;


public interface ProjectListWebService {

    @POST(Constants.URL_LIST)
    GetProjectListResponse getProjectList(@Body GetProjectListRequest body);

}
