package com.example.fcc.thecustomerapp.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;

import com.example.fcc.thecustomerapp.R;
import com.example.fcc.thecustomerapp.components.Constants;
import com.example.fcc.thecustomerapp.components.GroupItems;
import com.example.fcc.thecustomerapp.event.EventFragments;
import com.example.fcc.thecustomerapp.models.Rate;
import com.example.fcc.thecustomerapp.models.UserLogin;
import com.example.fcc.thecustomerapp.utilities.Util;
import com.example.fcc.thecustomerapp.views.RateChildView;
import com.example.fcc.thecustomerapp.views.RateChildView_;
import com.example.fcc.thecustomerapp.views.RatedChildView;
import com.example.fcc.thecustomerapp.views.RatedChildView_;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

public class AdapterExpandable extends BaseExpandableListAdapter {
    private ArrayList<GroupItems> grupos;
    private Context context;
    private UserLogin userLogin;

    public AdapterExpandable(Context context, ArrayList<GroupItems> grupos, UserLogin userLogin) {
        EventBus.getDefault().register(this);
        this.context = context;
        this.grupos = grupos;
        this.userLogin = userLogin;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return grupos.get(groupPosition).children.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        Rate children = (Rate) getChild(groupPosition, childPosition);
        RateChildView rateChildView;
        RatedChildView ratedChildView;

        if (convertView == null && children.getStatus().equals(Constants.RATE_STATUS_RATE)) {
            convertView = RateChildView_.build(context);
        }

        if (convertView == null && children.getStatus().equals(Constants.RATE_STATUS_RATED)) {
            convertView = RatedChildView_.build(context);
        }
        if (convertView == null && children.getStatus().equals(Constants.RATE_STATUS_NULL)) {
            convertView = RateChildView_.build(context);
        }

        if (children.getStatus().equals(Constants.RATE_STATUS_RATED)) {
            ratedChildView = (RatedChildView) convertView;
            ratedChildView.bind(groupPosition,childPosition,children,userLogin,grupos);

        } else if (children.getStatus().equals(Constants.RATE_STATUS_RATE)) {

            rateChildView = (RateChildView) convertView;
            rateChildView.bind(groupPosition, childPosition, children, userLogin, grupos, true);

        } if (children.getStatus().equals(Constants.RATE_STATUS_NULL)) {

            rateChildView = (RateChildView) convertView;
            rateChildView.bind(groupPosition, childPosition, children, userLogin, grupos, false);
        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return grupos.get(groupPosition).children.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return grupos.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return grupos.size();
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.items_rate, null);
        }

        GroupItems grupo = (GroupItems) getGroup(groupPosition);

        ((CheckedTextView) convertView).setText(grupo.string);
        ((CheckedTextView) convertView).setChecked(isExpanded);

        if (((CheckedTextView) convertView).isChecked()) {
            ((CheckedTextView) convertView).setCheckMarkDrawable(R.drawable.icon_flecha_2);

        } else {
            ((CheckedTextView) convertView).setCheckMarkDrawable(R.drawable.icon_flecha_1);
            Util.message(grupos.get(groupPosition).children.size()+"");

        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Subscribe
    public void onEvent(EventFragments event){
        if (event.getTag().equals(Constants.TAG_RATED_CONTROL)){
            grupos.get(event.getGroupPosition()).children.remove(event.getChildPosition());

            if (grupos.get(event.getGroupPosition()).children.size()<1){
                Rate rated = new Rate();
                rated.setStatus(Constants.RATE_STATUS_NULL);
                rated.setId(Constants.RATE_STATUS_NULL);
                grupos.get(event.getGroupPosition()).children.add(rated);
            }

            notifyDataSetChanged();
        }
    }

}