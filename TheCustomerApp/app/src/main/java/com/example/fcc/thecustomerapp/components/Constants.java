package com.example.fcc.thecustomerapp.components;


/**
 * Created by fcc on 20/01/16.
 */
public class Constants {
    //DOWNLOAD IMAGE
    public static final String DOWNLOAD_ERROR = "Error cargando la imagen: ";

    //URL BASE
    public static final String URL_BASE = "http://jaguarlabs.com/sistemas/acaponeta/ws/";

    //ERRORES
    public static final String ERROR_JSON_CODE_300 = "Solicitud erronea";
    public static final String ERROR_JSON_CODE_301 = "Datos insuficientes";
    public static final String ERROR_JSON_CODE_302 = "Error, el email no es de un usuario valido";
    public static final String ERROR_JSON_CODE_303 = "Error al enviar el nuevo password por email";
    public static final String ERROR_ROBOSPICE_PETICION = "Fallo la petición web";

    //FRAGMENTS TAGS
    public static final String TAG_SPLASH = "tag_splash";
    public static final String TAG_FORGOT = "tag_forgot";
    public static final String TAG_FORGOT_ACCEPT = "tag_forgot_accept";
    public static final String TAG_FORGOT_CANCEL = "tag_forgot_cancel";
    public static final String TAG_FORGOT_REINTENT = "tag_forgot_reintent";
    public static final String TAG_LIST = "tag_lista";
    public static final String TAG_LOGIN = "tag_login";
    public static final String TAG_DETAILS = "tag_details";
    public static final String TAG_RATE = "tag_rate";
    public static final String TAG_RATED = "tag_rated";
    public static final String TAG_RATED_MSG = "tag_rated_msg";
    public static final String TAG_RATED_CONTROL = "tag_rated_control";
    public static final String TAG_CLOSE = "tag_close";
    public static final String TAG_CLOSE_ACCEPT = "tag_close_accept";
    public static final String TAG_CLOSE_CANCEL = "tag_close_cancel";
    public static final String TAG_CORRECT_RESTORE = "tag_correct_restore";
    public static final String TAG_INCORRECT_RESTORE = "tag_incorrect_restore";

    //SPLASH
    public static final long SPLASH_SCREEN_DELAY = 2000;

    //TECNOLOGY
    public static final String TEC_ANDROID = "Android";
    public static final String TEC_IOS = "iOS";
    public static final String TEC_WEB = "Web";

    public static final String URL_LOGIN = "/login.php";
    public static final String MESSAGE_FORGOT = "Olvide mi contraseña";

    //DETAILS
    public static final String URL_DETAILS = "/proyecto.php";
    public static final String DETAILS_DATE_SEPARATOR = "/";

    //LIST
    public static final String URL_LIST = "/proyecto.php";

    //RESTORE
    public static final String URL_RESTORE = "/usuario.php";

    //TOOLBAR LIST
    public static final String TITLE_TOOLBAR_LIST = "   Proyectos";

    //TOOLBAR RATE
    public static final String TITLE_TOOLBAR_RATE = "   Calificar";

    //TOOLBAR LIST
    public static final String TITLE_TOOLBAR_DETAILS = "   Detalle de proyectos";

    //TEST CONEXION
    public static final String MSG_TEST_CONEXION = " Comprueba tu conexión a Internet ...";

    //RATE
    public static final String URL_RATE_MSG = "/proyecto.php";
    public static final String URL_RATED = "/proyecto.php";
    public static final String URL_RATE = "/proyecto.php";
    public static final String RATE_STATUS_RATE = "evaluacion";
    public static final String RATE_STATUS_RATED = "historial";
    public static final String RATE_STATUS_NULL = "Null";
    public static final String RATE_TEC_ANDROID = " Android";
    public static final String RATE_TEC_IOS = " iOS";
    public static final String RATE_TEC_WEB = " Web";
    public static final String RATE_RESULT_MSG = "resultado";

    //MD5
    public static final String MD5_TITTLE = "MD5";
    public static final String MD5_UTF = "UTF-8";
    public static final String MD5_NO_SUCH_ALGORITHM_EXCEPTION = "Huh, MD5 should be supported?";
    public static final String MD5_UNSUPPORTED_ENCODING_EXCEPTION = "UnsupportedEncodingException";

    //JSON
    public static final String JSON_SOLICITUDE = "solicitud";
    public static final String JSON_SOLICITUDE_LIST = "listado";
    public static final String JSON_SOLICITUDE_PROYECTS = "proyectos";
    public static final String JSON_SOLICITUDE_DETAIL = "detalle";
    public static final String JSON_SOLICITUDE_PROYECT = "proyecto";
    public static final String JSON_SOLICITUDE_CONTACT = "contacto";
    public static final String JSON_SOLICITUDE_TECNOLOGYS = "tecnologias";
    public static final String JSON_SOLICITUDE_RECOVER = "recuperar_password";
    public static final String JSON_SOLICITUDE_RATE = "por_evaluar";
    public static final String JSON_SOLICITUDE_QUALIFY = "por_calificar";
    public static final String JSON_SOLICITUDE_RATED = "historial_evaluaciones";
    public static final String JSON_SOLICITUDE_RATED_ELEMENT = "historial";
    public static final String JSON_SOLICITUDE_RATED_MSG = "evaluar";

    public static final String JSON_TECNOLOGYS_ID = "id";
    public static final String JSON_TECNOLOGYS_TYPE_TECNOLOGY = "tecnologia";
    public static final String JSON_TECNOLOGYS_SPRINT = "sprint";
    public static final String JSON_TECNOLOGYS_RESTRICTIONS = "restricciones";
    public static final String JSON_TECNOLOGYS_BEGIN = "inicio";
    public static final String JSON_TECNOLOGYS_NEXT_DELIVER = "proxima_entrega";
    public static final String JSON_TECNOLOGYS_STATUS = "estatus";
    public static final String JSON_TECNOLOGYS_TYPE = "tipo";
    public static final String JSON_TECNOLOGYS_RATE = "calificacion";

    public static final String JSON_COMMENTS = "comentarios";
    public static final String JSON_CONTACT_EMAIL = "email";
    public static final String JSON_CONTACT_ROLE = "rol";
    public static final String JSON_LOGIN = "login";
    public static final String JSON_PASS = "password";
    public static final String JSON_CODE = "codigo";
    public static final String JSON_MSG = "mensaje";
    public static final String JSON_USER = "usuario";
    public static final String JSON_ID_USER = "id_usuario";
    public static final String JSON_ID_CLIENT = "id_cliente";

    public static final String JSON_PROYECT_ID = "id_proyecto";
    public static final String JSON_PROYECT_NAME = "nombre";
    public static final String JSON_CONTACT_NAME = "nombre";

    public static final String JSON_PROYECT_BEGIN_DATE = "fecha_inicio";
    public static final String JSON_PROYECT_END_DATE = "fecha_finalizacion";
    public static final String JSON_PROYECT_IMAGE = "imagen";

    public static final String JSON_COMPLETE_NAME_USER = "nombre_completo_usuario";
    public static final String JSON_TOKEN = "token";
    public static final String JSON_REFRESH_TOKEN = "refresh_token";

    public static final String JSON_RATED_MSG_ID = "id";
    public static final String JSON_RATED_MSG_RATE = "calificacion";
    public static final String JSON_RATED_MSG_TOKEN = "token";
    public static final String JSON_RATED_MSG_REFRESHTOKEN = "refresh_token";
}
